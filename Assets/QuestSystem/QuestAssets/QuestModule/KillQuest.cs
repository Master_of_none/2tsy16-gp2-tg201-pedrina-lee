﻿using UnityEngine;
using System.Collections;

public class KillEvent : GameEvent
{
	public string MonsterID;
}

public class KillQuest : Quest
{
	public string MonsterID;
	public int RequiredKillCount;
	public int Progress;

	void OnEnable()
	{
		this.AddEventListenerGlobal<KillEvent> (OnMonsterKilled);
	}

	void OnDisable()
	{
		this.RemoveEventListenerGlobal<KillEvent> (OnMonsterKilled);
	}

	void OnMonsterKilled(KillEvent eventDetails)
	{
		// Was the monster that died same as the monster required in this quest?
		if (eventDetails.MonsterID == MonsterID) {
			Progress++;
			if (IsComplete()) {
				Complete();
			}
		}
        else if(eventDetails.MonsterID == "Any")
        {
            Progress++;
            if(IsComplete())
            {
                Complete();
            }
        }
	}

	public override bool IsComplete()
	{
		if (Progress >= RequiredKillCount)
			return true;
		else
			return false;
	}

	public override void Complete()
	{
		GameObject playerToReward = QuestActor;
		// TODO: Reward the player here

		// Sample implementation
		// This assumes that the player has Level and Player components
		// NOTE THAT THIS IS A SAMPLE ONLY
		// The code below may change depending on how you coded your own game
		playerToReward.GetComponent<UnitStatManager> ().ReceiveExp (ExperienceReward);
		playerToReward.GetComponent<UnitStatManager> ().ReceiveGold (GoldReward);

        QuestCompletedEvent completedQuestDetails = new QuestCompletedEvent();
        completedQuestDetails.Quest = this;
        this.RaiseEventGlobal<QuestCompletedEvent>(completedQuestDetails);

		base.Complete ();
	}
}
