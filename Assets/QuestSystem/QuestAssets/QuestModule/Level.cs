﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {
	public int Exp;

	public void AddXp(int exp)
	{
		Exp += exp;
	}
}
