﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour
{
	public string MonsterId;
	public int Health;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		// Test code, reduce our HP by 10 per mouse click
		if (Input.GetMouseButtonDown (0)) {
			TakeDamage(10);
		}

	}

	void TakeDamage(int damage)
	{
		Health -= damage;
		if (Health <= 0) {
			// We are dead, broadcast an event
			Health = 0;
			KillEvent killEventDetails = new KillEvent();
			killEventDetails.MonsterID = MonsterId;
			this.RaiseEventGlobal<KillEvent>(killEventDetails);

			Destroy(gameObject);
		}

	}
}
