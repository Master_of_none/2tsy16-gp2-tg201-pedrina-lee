﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public int Gold;

	public void AddGold(int gold)
	{
		Gold += gold;
	}
}
