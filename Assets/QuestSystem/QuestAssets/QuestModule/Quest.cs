﻿using UnityEngine;
using System.Collections;

public class Quest : MonoBehaviour
{
    public string QuestName;
	public GameObject QuestActor; // The one who accepted the quest, in this case the player
	public int ExperienceReward;
	public int GoldReward;
    public string QuestDescription;

	public virtual void Complete() 
	{

	}
	public virtual bool IsComplete() 
	{
		return false;
	}

	void Start()
	{
		// WARNING: This quest script expects that the player is tagged with "Player" to work
		QuestActor = GameObject.FindGameObjectWithTag ("Player");
	}
}
