﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestCompletedEvent : GameEvent
{
	public Quest Quest;
}


public class QuestManager : MonoBehaviour
{
	public List<Quest> ActiveQuests;

	void OnEnable()
	{
		this.AddEventListenerGlobal<QuestCompletedEvent> (OnQuestCompleted);
	}

	void OnDisable()
	{
		this.AddEventListenerGlobal<QuestCompletedEvent> (OnQuestCompleted);
	}


	void OnQuestCompleted(QuestCompletedEvent eventDetails)
	{
		// Is the completed quest in our list of active quests?
		Quest questToRemove = null;
		foreach (Quest activeQuest in ActiveQuests) {
			if (eventDetails.Quest == activeQuest) {
				questToRemove = activeQuest;
				// Stop checking the list since we already found a match
				break;
			}
		}

		ActiveQuests.Remove (questToRemove);
		Destroy (questToRemove.gameObject);
	}
}
