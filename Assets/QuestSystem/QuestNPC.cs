﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuestNPC : MonoBehaviour {
    public Quest QuestToGive;
    public QuestManager Manager;
    public GameObject QuestUI;
    public List<GameObject> QuestPanels;
    private bool interactable;

	// Use this for initialization
	void Start () {
        Manager = GameObject.FindGameObjectWithTag("QuestManager").GetComponent<QuestManager>() ;
        QuestUI = GameObject.FindGameObjectWithTag("QuestUI");
        foreach(Transform ui in QuestUI.transform)
        {
            if(ui.name.Contains("QuestPanel"))
            {
                QuestPanels.Add(ui.gameObject);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        Interact();
	}

    public void Interact()
    {
        if(Input.GetKeyDown(KeyCode.E) && interactable)
        {
            print("Receive Quest");
            Manager.ActiveQuests.Add(QuestToGive);
            DisplayQuests();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        interactable = true;
    }

    public void OnTriggerExit(Collider other)
    {
        interactable = false;
    }

    public void DisplayQuests()
    {

        foreach (Quest quest in Manager.ActiveQuests)
        {
            foreach (GameObject panel in QuestPanels)
            {
                if (panel.transform.FindChild("QuestName").GetComponent<Text>().text == "")
                {
                    panel.transform.FindChild("QuestName").GetComponent<Text>().text = QuestToGive.QuestName;
                    panel.transform.FindChild("QuestDesc").GetComponent<Text>().text = QuestToGive.QuestDescription;
                    break;
                }
            }
        }
    }
}
