﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ItemDatabase : ScriptableObject {

    [SerializeField]
    private List<Item> Database;
    [SerializeField]
    public static ItemDatabase Instance;


    void OnEnable()
    {
        if (Database == null)
            Database = new List<Item> ();
    }

    public void Add(Item item)
    {
        Database.Add(item);
    }

    public void Remove(Item item)
    {
        Database.Remove(item);
    }

    public void RemoveAt(int index)
    {
        Database.RemoveAt(index);
    }

    public int Count
    {
        get { return Database.Count; }
    }

    //.ElementAt() requires the System.Linq
    public Item GetItem(int index)
    {
        return Database.ElementAt(index);
    }

    public Item GetItem(string name)
    {
        return Database.Find(x => x.ItemName == name);
    }

    public void SortAlphabeticallyAtoZ()
    {
        Database.Sort((x, y) => string.Compare(x.ItemName, y.ItemName));
    }
}
