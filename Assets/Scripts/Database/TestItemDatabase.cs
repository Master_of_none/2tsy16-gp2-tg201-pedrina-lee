﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TestItemDatabase : MonoBehaviour {

    [SerializeField]
    public List<GameObject> Database;
    [SerializeField]
    public static TestItemDatabase Instance;


    void OnEnable()
    {
        if (Database == null)
            Database = new List<GameObject> ();
    }

    public void Add(GameObject item)
    {
        Database.Add(item);
    }

    public void Remove(GameObject item)
    {
        Database.Remove(item);
    }

    public void RemoveAt(int index)
    {
        Database.RemoveAt(index);
    }

    public int Count
    {
        get { return Database.Count; }
    }

    //.ElementAt() requires the System.Linq
    public GameObject GetItemObject(int index)
    {
        return Database.ElementAt(index);
    }

    public BaseItem GetItem(int index)
    {
        return Database.ElementAt(index).GetComponent(typeof(BaseItem)) as BaseItem;
    }

    public GameObject GetItem(string name)
    {
        return Database.Find(x => (x.GetComponent(typeof(BaseItem)) as BaseItem).Name == name);
    }

    public void SortAlphabeticallyAtoZ()
    {
        Database.Sort((x, y) => string.Compare((x.GetComponent(typeof(BaseItem)) as BaseItem).Name, (y.GetComponent(typeof(BaseItem)) as BaseItem).Name));
    }
}