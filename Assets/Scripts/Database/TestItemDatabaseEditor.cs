﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;

//[CanEditMultipleObjects, CustomEditor(typeof(NewItemDatabase))]
public class TestItemDatabaseEditor : Editor
{

    private enum State
    {
        Main,
        AddHealingItem
    }
    private string status = "Item Database";
    private bool showField = false;
    private State state;
    private List<SerializedProperty> properties;
    private NewItemDatabase itemDatabase;

    void OnEnable()
    {
        itemDatabase = serializedObject as System.Object as NewItemDatabase;
        properties = new List<SerializedProperty>();
        SerializedProperty temp = serializedObject.GetIterator();
        while (temp.NextVisible(true))
        {
            properties.Add(temp);
        }
        Debug.Log(properties.Count);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        
        switch (state)
        {
            case State.Main:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("Items"));
                EditorGUILayout.Foldout(showField, status);
                
                EditorGUILayout.BeginVertical(GUILayout.Width(250), GUILayout.Height(300));
                EditorGUILayout.Space();/*
                Debug.Log(itemDatabase.Count);
                for (int cnt = 0; cnt < itemDatabase.Count; cnt++)
                {
                    EditorGUILayout.BeginHorizontal();
                    if (GUILayout.Button("-", GUILayout.Width(25)))
                    {
                        itemDatabase.RemoveAt(cnt);
                        state = State.Main;
                        return;
                    }
                    if (GUILayout.Button((itemDatabase.GetItem(cnt).GetComponent(typeof(BaseItem)) as BaseItem).Name, "box", GUILayout.ExpandWidth(true)))
                    {
                        //selectedItem = cnt;
                        //state = State.Edit;
                    }
                    EditorGUILayout.EndHorizontal();
                }*/

                if (GUILayout.Button("Add Healing Item", GUILayout.Width(125)))
                {
                    state = State.AddHealingItem;     
                }
                break;
            case State.AddHealingItem:
                //GameObject tempGo = new GameObject();
                //HealingItem temp = tempGo.AddComponent<HealingItem>();
                HealingItem temp = new HealingItem();
                int ItemID = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Item ID: "), temp.ItemID.ToString()));
                string Name = EditorGUILayout.TextField(new GUIContent("Item Name: "), temp.Name);
                string Description = EditorGUILayout.TextField(new GUIContent("Item Description"), temp.Description);
                int Cost = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Item Cost: "), temp.Cost.ToString()));
                Sprite ItemSprite = (Sprite)EditorGUILayout.ObjectField("Sprite: ", temp.ItemSprite, typeof(Sprite), true);
                int RecoveryValue = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Recovery Value: "), temp.RecoveryValue.ToString()));
                if (GUILayout.Button("Done", GUILayout.Width(100)))
                {
                    GameObject tempGo = new GameObject();
                    HealingItem tempHI = tempGo.AddComponent<HealingItem>();
                    tempHI.ItemID = ItemID;
                    tempHI.Name = Name;
                    tempHI.Description = Description;
                    tempHI.Cost = Cost;
                    tempHI.ItemSprite = ItemSprite;
                    tempHI.RecoveryValue = RecoveryValue;
                    itemDatabase.AddItem(tempGo);   
                    state = State.Main;
                }
                if(GUILayout.Button("Cancel", GUILayout.Width(100)))
                {
                    state = State.Main;
                }
                break;
        }
    }

}
