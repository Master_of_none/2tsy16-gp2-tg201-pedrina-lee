﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class OnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateBehaviour>().ToggleMovement();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateBehaviour>().ToggleMovement();
    }
}
