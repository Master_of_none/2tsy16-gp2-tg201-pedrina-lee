﻿using UnityEngine;
using System.Collections;

public class PlayAction : MonoBehaviour {

    public Animator MyAnimator;

    public void Start()
    {
        MyAnimator = GetComponent<Animator>();
    }

    public void PlayAnimation(string action)
    {
        MyAnimator.SetTrigger(action);
    }

    public void DamageTo() { }
    public void StopEffect() { }
    public void StartEffect() { }
    public void AreaAttack() { }
}
