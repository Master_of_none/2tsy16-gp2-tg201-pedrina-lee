﻿using UnityEngine;
using System.Collections;

public class ToggleUI : MonoBehaviour {

    public void Toggle(GameObject gameObject)
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
