﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public Vector3 PositionOffset;
	public float Damping;
    public GameObject Player;

	// Use this for initialization
	void Start () {
        //Alternative GameObject.FindGameObjectWithTag("Player");
        Player = GameObject.Find("Koko");
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 desiredPosition = Player.transform.position + PositionOffset;
		transform.position = Vector3.Lerp (transform.position, desiredPosition, Time.deltaTime * Damping);
	}
}
