﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayDamageText : MonoBehaviour {
    public Canvas Damage;
    private GameObject damageTexts;
	// Use this for initialization
	void Start () {
        damageTexts = GameObject.FindGameObjectWithTag("DamageText");
	}
	
	public void DisplayDamage(int damage)
    {
        Damage.gameObject.transform.Find("Text").GetComponent<Text>().text = "-" + damage.ToString();
        GameObject go = Instantiate(Damage, transform.position, Quaternion.identity) as GameObject;
        go.transform.SetParent(damageTexts.transform);
    }
}
