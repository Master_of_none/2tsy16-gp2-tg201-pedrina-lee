﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisplayUI : MonoBehaviour {
    
    public List<GameObject> UIs;
    public GameObject Player;

	// Use this for initialization
	void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");
        foreach(Transform ui in transform)
        {
            UIs.Add(ui.gameObject);
            ui.gameObject.SetActive(false);
        }
        //UI.SetActive(!UI.activeSelf);
    }
	
	// Update is called once per frame
	void Update () {
        ToggleUI();
	}

    public void ToggleUI()
    {
        for (int i = 0; i < UIs.Count; i++)
        {
            if (Input.inputString.ToLower() == UIs[i].name[1].ToString().ToLower())
            {
                UIs[i].SetActive(!UIs[i].activeSelf);
                if (!Player.GetComponent<PlayerStateBehaviour>().CanMove && !UIs[i].activeSelf)
                    Player.GetComponent<PlayerStateBehaviour>().ToggleMovement();
            }
        }
    }
}
