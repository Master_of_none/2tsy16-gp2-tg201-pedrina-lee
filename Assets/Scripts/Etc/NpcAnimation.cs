﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NpcAnimation : MonoBehaviour {

    public Animator MyAnimator;

	// Use this for initialization
	void Start ()
    {
        MyAnimator = GetComponent<Animator>();
	}
 	
	public void LoadScene(string sceneName)
	{
		SceneManager.LoadScene (sceneName);
	}

    public void PlayAction(string action)
    {
        MyAnimator.SetTrigger(action);
    }

}
