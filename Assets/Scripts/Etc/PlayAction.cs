﻿using UnityEngine;
using System.Collections;

public class PlayAnimation : MonoBehaviour {

    public Animator MyAnimator;

    public void Start()
    {
        MyAnimator = GetComponent<Animator>();
    }

    public void PlayAction(string action)
    {
        MyAnimator.SetTrigger(action);
    }
}
