﻿using UnityEngine;
using System.Collections;

public class PointSpawn : MonoBehaviour {

	public GameObject PointModel;
	public LayerMask Grounded;

	private PlayerMovement playerMovement;
	private Transform playerTransform;

	void Awake()
	{
		playerMovement = GetComponent<PlayerMovement> ();
		playerTransform = GetComponent<Transform> ();
	}

	// Use this for initialization
	void Start () {
		PointModel = Instantiate (PointModel) as GameObject;
		PointModel.transform.position = playerTransform.position;
		PointModel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		GetSpawnPoint ();
		DestroyPoint ();
	}

	void DestroyPoint()
	{
		if (Vector3.Distance (transform.position, PointModel.transform.position) < playerMovement.StoppingDistance) {
			PointModel.SetActive (false);
		}
	}

	void GetSpawnPoint()
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray, out hit, Mathf.Infinity, Grounded))
			{
				PointModel.transform.position = hit.point + new Vector3(0.0f, 0.2f, 0.0f);
				PointModel.SetActive (true);
			}
		}
	}
}
