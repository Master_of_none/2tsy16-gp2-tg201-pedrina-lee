﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Spawning : MonoBehaviour {

    public GameObject SpawnParent;
	public List<GameObject> Enemies;
	public int MaxSpawn;
	public float SpawnInterval;
	public float MaxSpawnRange;

	[SerializeField]private List<GameObject> spawnedEnemies;
	private Transform myTransform;
    private float lastSpawn;

	// Use this for initialization
	void Awake () {
		spawnedEnemies = new List<GameObject> ();
		myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
        RandomSpawn();
        UpdateList();
	}

    void UpdateList()
    {
        foreach(GameObject enemy in spawnedEnemies)
        {
            if(enemy == null)
            {
                spawnedEnemies.Remove(enemy);
                break;
            }
        }
    }

	void RandomSpawn()
	{
        if(spawnedEnemies.Count >= MaxSpawn)
        {
            lastSpawn = Time.time;
        }
        else if (spawnedEnemies.Count < MaxSpawn && Time.time > lastSpawn + SpawnInterval ) {
			Vector3 RandomPosition = new Vector3 (Random.Range (-MaxSpawnRange, MaxSpawnRange), myTransform.position.y, Random.Range (-MaxSpawnRange, MaxSpawnRange));
			int randomIndex = Random.Range (0, Enemies.Count);
			GameObject spawned = Instantiate (Enemies [randomIndex], myTransform.position + RandomPosition, Quaternion.identity) as GameObject;
			spawnedEnemies.Add (spawned);
            spawned.transform.SetParent(SpawnParent.transform);
            lastSpawn = Time.time;
		}
	}

}
