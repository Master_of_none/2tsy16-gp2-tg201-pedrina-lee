﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class InventoryCell : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler {

    public Item Item;
    public GameObject player;
    public GameObject inventory;
    public Text description;
    
    //private Inven
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        inventory = GameObject.FindGameObjectWithTag("Inventory");
    }

    // Update is called once per frame
    void Update()
    {
        if (Item != null)
        {
            GetComponentInChildren<Image>().sprite = Item.ItemSprite;
        }
        else
            GetComponentInChildren<Image>().sprite = null;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //Item.Use();
        player.GetComponent<TestPlayerInventory>().ItemInventory.Remove(Item);
        inventory.GetComponent<TestInventory1>().ItemInventory.Remove(Item);
        Destroy(Item.ItemModel);
        //Tooltip
        //GetComponentInParent<Inventory>().SelectedItem = gameObject;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //description.text = Item.description;
    }

    public void OnPointExit(PointerEventData eventData)
    {
        //description.text = null;
    }
}