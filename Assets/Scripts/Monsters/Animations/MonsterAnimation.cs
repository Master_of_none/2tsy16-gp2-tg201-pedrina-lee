﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MonsterAnimation : MonoBehaviour {

    public Animator MyAnimator;

	// Use this for initialization
	void Start () {
        MyAnimator = GetComponent<Animator>();
	}

    public void SelectScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void PlayAction(string action)
    {
        MyAnimator.SetTrigger(action);
    }

    public void DamageTo()
    {
        
    }
}
