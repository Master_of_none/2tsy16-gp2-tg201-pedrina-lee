﻿using UnityEngine;
using System.Collections;

public interface IEnemyState {

    void ToAttackState();

    void ToChaseState();

    void ToIdleState();

    void ToPatrolState();

    void ToDeadState();

    void UpdateState();


}
