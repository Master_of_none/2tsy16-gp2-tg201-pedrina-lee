﻿using UnityEngine;
using System.Collections;

public class StatePatternEnemy : MonoBehaviour {
    public string MonsterID;
    public bool isDead;
    public float AttackSpeed;
    public float AttackRange;
    public float CastRange;
    public float PatrolRange;
    public float SearchRadius;
    public float SearchDuration;

    
	[HideInInspector] public IEnemyState CurrentState;
    [HideInInspector] public IdleState myIdleState;
    [HideInInspector] public AttackState myAttackState;
    [HideInInspector] public ChaseState myChaseState;
    [HideInInspector] public PatrolState myPatrolState;
    [HideInInspector] public DeadState myDeadState;
    [HideInInspector] public GameObject Target;
    [HideInInspector] public NavMeshAgent NavAgent;
    [HideInInspector] public Transform ChaseTarget;
    [HideInInspector] public Transform myTransform;
    [HideInInspector] public Animator MyAnimator;

    private UnitStatManager statManager;

    void Awake()
    {
        myIdleState = new IdleState(this);
        myAttackState = new AttackState(this);
        myChaseState = new ChaseState(this);
        myPatrolState = new PatrolState(this);
        myDeadState = new DeadState(this);

        statManager = GetComponent<UnitStatManager>();
		MyAnimator = GetComponent<Animator> ();
        myTransform = transform;
		NavAgent = GetComponent<NavMeshAgent> ();
    }

    // Use this for initialization
    void Start () {
        CurrentState = myPatrolState;
    }
	
	// Update is called once per frame
	void Update () {
        CurrentState.UpdateState();
        if(isDead)
        {
            CurrentState = myDeadState;
        }
	}

    public void Dead()
    {
        if (transform.FindChild("AttackMarker"))
        {
            transform.FindChild("AttackMarker").gameObject.SetActive(false);
            transform.FindChild("AttackMarker").SetParent(null);
        }
        Destroy(gameObject, 1f);
    }

    public void OnDestroy()
    {
        if(Target != null)
            Target.GetComponent<UnitStatManager>().ReceiveExp(statManager.RandomExp());

        KillEvent killEventDetails = new KillEvent();
        killEventDetails.MonsterID = MonsterID;
        this.RaiseEventGlobal<KillEvent>(killEventDetails);
    }
    
     
}
