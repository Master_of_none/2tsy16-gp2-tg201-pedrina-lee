﻿using UnityEngine;
using System.Collections;

public class AttackState : IEnemyState {

    public readonly StatePatternEnemy enemy;
    private float lastAttack;

    public AttackState(StatePatternEnemy statePatternEnemy)
    {
        enemy = statePatternEnemy;
    }

    public void UpdateState()
    {
        Attack();
        OutOfRange();
    }

    public void ToDeadState()
    {
        enemy.CurrentState = enemy.myDeadState;
    }

    public void ToAttackState()
    {
        Debug.Log("Cannot switch to Attack State");
    }

    public void ToChaseState()
    {
        enemy.CurrentState = enemy.myChaseState;
    }

    public void ToIdleState()
    {
        enemy.CurrentState = enemy.myIdleState;
    }

    public void ToPatrolState()
    {
        enemy.CurrentState = enemy.myPatrolState;
    }

    private void Attack()
    {
        enemy.MyAnimator.SetBool("AttackState", true);
        enemy.NavAgent.Resume();
        Quaternion LookAt = Quaternion.LookRotation(enemy.Target.transform.position - enemy.myTransform.position);
        LookAt.z = 0;
        LookAt.x = 0;
        enemy.myTransform.rotation = Quaternion.Slerp(enemy.myTransform.rotation, LookAt, enemy.NavAgent.angularSpeed * Time.deltaTime);

        if (Time.time > lastAttack + enemy.AttackSpeed) 
        {
            lastAttack = Time.time;
            enemy.MyAnimator.SetTrigger("Attack");
            //PlayAttackAnimation
        }
    }

    private void OutOfRange()
    {
        if (Vector3.Distance(enemy.Target.transform.position, enemy.myTransform.position) > enemy.AttackRange)
        {
            enemy.MyAnimator.SetBool("AttackState", false);
            ToChaseState();
        }
    }

}
