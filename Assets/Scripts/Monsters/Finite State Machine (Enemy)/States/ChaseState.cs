﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ChaseState : IEnemyState {

    public readonly StatePatternEnemy enemy;

    public ChaseState(StatePatternEnemy statePatternEnemy)
    {
        enemy = statePatternEnemy;
    }

    public void UpdateState()
    {
        Chase();
    }

    public void ToAttackState()
    {
        enemy.CurrentState = enemy.myAttackState;
    }

    public void ToChaseState()
    {
        Debug.Log("Cannot switch to Chase State");
    }

    public void ToIdleState()
    {
        enemy.CurrentState = enemy.myIdleState;
    }

    public void ToPatrolState()
    {
        enemy.CurrentState = enemy.myPatrolState;
    }

    public void ToDeadState()
    {
        enemy.CurrentState = enemy.myDeadState;
    }

    private void Chase()
    {
		if (enemy.Target != null) {
            enemy.MyAnimator.SetBool("ChaseState", true);
			if (Vector3.Distance (enemy.myTransform.position, enemy.Target.transform.position) > enemy.SearchRadius) {
                enemy.MyAnimator.SetBool("ChaseState", false);
                enemy.Target = null;
				enemy.NavAgent.SetDestination (enemy.myTransform.position);
				ToIdleState ();
				return;
			} else if (Vector3.Distance (enemy.myTransform.position, enemy.Target.transform.position) <= enemy.AttackRange) {
                enemy.MyAnimator.SetBool("ChaseState", false);
                enemy.NavAgent.Stop();
				ToAttackState ();
				return;
			}
			enemy.NavAgent.Resume ();	
			enemy.NavAgent.destination = enemy.Target.transform.position;
		}
    }
}
