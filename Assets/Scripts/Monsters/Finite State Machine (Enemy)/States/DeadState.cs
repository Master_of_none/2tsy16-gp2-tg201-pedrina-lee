﻿using UnityEngine;
using System.Collections;
using System;

public class DeadState : IEnemyState
{
    public readonly StatePatternEnemy enemy;

    public DeadState() { }
    public DeadState(StatePatternEnemy statePatternEnemy)
    {
        enemy = statePatternEnemy;
    }

    public void ToAttackState()
    {
        enemy.CurrentState = enemy.myAttackState;
    }

    public void ToChaseState()
    {
        enemy.CurrentState = enemy.myChaseState;
    }

    public void ToIdleState()
    {
        enemy.CurrentState = enemy.myIdleState;
    }

    public void ToPatrolState()
    {
        enemy.CurrentState = enemy.myDeadState;
    }

    public void ToDeadState()
    {
        Debug.Log("Cannot transition into the same state");
    }


    public void UpdateState()
    {
        enemy.MyAnimator.SetBool("Dead", true);
        enemy.Dead();
    }
}
