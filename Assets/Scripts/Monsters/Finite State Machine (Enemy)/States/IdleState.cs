﻿using UnityEngine;
using System.Collections;

public class IdleState : IEnemyState {

    private readonly StatePatternEnemy enemy;
    private float waitTime;

    public IdleState(StatePatternEnemy statePatternEnemy)
    {
        enemy = statePatternEnemy;
    }

    public void UpdateState()
    {
        waitTime += Time.deltaTime;
        Standby();
		DetectNearbyPlayer ();
    }

    public void ToDeadState()
    {
        enemy.CurrentState = enemy.myDeadState;
    }

    public void ToAttackState()
    {
        enemy.CurrentState = enemy.myAttackState;
        waitTime = 0;
    }

    public void ToChaseState()
    {
        enemy.CurrentState = enemy.myChaseState;
        waitTime = 0;
    }

    public void ToIdleState()
    {
        Debug.Log("Cannot switch to idle state");
    }

    public void ToPatrolState()
    {
        enemy.CurrentState = enemy.myPatrolState;
        waitTime = 0;
    }

    private void Standby()
    {
        enemy.MyAnimator.SetBool("IdleState", true);
        if (waitTime >= enemy.SearchDuration)
        {
            enemy.MyAnimator.SetBool("IdleState", false);
            ToPatrolState();
        }
    }

	private void DetectNearbyPlayer()
	{
		Collider[] colliders = Physics.OverlapSphere (enemy.myTransform.position, enemy.SearchRadius);
		foreach (Collider col in colliders) {
			if (col.gameObject.CompareTag ("Player")) {
                enemy.MyAnimator.SetBool("IdleState", false);
                ToChaseState ();
				enemy.Target = col.gameObject;
                return;
			}
		}
	}

}
