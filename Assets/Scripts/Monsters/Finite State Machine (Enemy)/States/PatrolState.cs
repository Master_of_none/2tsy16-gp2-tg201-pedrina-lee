﻿using UnityEngine;
using System.Collections;
using System;

public class PatrolState : IEnemyState {

    public readonly StatePatternEnemy enemy;
    private Vector3 destination;

    public PatrolState(StatePatternEnemy statePatternEnemy)
    {
        enemy = statePatternEnemy;
        destination = enemy.myTransform.position;
    }

    public void UpdateState()
    {
        Patrol();
		DetectNearbyPlayer ();
    }

    public void ToAttackState()
    {
        enemy.CurrentState = enemy.myAttackState;
    }

    public void ToChaseState()
    {
        enemy.CurrentState = enemy.myChaseState;
    }

    public void ToIdleState()
    {
        enemy.CurrentState = enemy.myIdleState;
    }

    public void ToPatrolState()
    {
        Debug.Log("Cannot switch to Patrol State");
    }

    private void Patrol()
    {
        enemy.NavAgent.destination = destination;
        enemy.NavAgent.Resume();
        enemy.MyAnimator.SetBool("PatrolState", true);

        if (enemy.NavAgent.remainingDistance <= enemy.NavAgent.stoppingDistance && !enemy.NavAgent.pathPending)
        {
            enemy.MyAnimator.SetBool("PatrolState", false);
            ToIdleState();
            Vector3 RandomRange = new Vector3(UnityEngine.Random.Range(-enemy.PatrolRange, enemy.PatrolRange), 0, UnityEngine.Random.Range(-enemy.PatrolRange, enemy.PatrolRange));
            destination = enemy.myTransform.position + RandomRange;
        }

    }

	private void DetectNearbyPlayer()
	{
		Collider[] colliders = Physics.OverlapSphere (enemy.myTransform.position, enemy.SearchRadius);
		foreach (Collider col in colliders) {
			if (col.gameObject.CompareTag ("Player")) {
                enemy.MyAnimator.SetBool("PatrolState", false);
                ToChaseState ();
				enemy.Target = col.gameObject;
                return;
			}
		}
	}

    public void ToDeadState()
    {
        enemy.CurrentState = enemy.myDeadState;
    }
}
