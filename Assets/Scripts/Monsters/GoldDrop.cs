﻿using UnityEngine;
using System.Collections;

public class GoldDrop : MonoBehaviour {

    public Vector2 GoldDropRange;
    public GameObject GoldPrefab;
    public StatePatternEnemy Enemy;
    private bool goldDropped;

	// Use this for initialization
	void Start () {
        Enemy = GetComponent<StatePatternEnemy>();
        goldDropped = false;
	}
	
	// Update is called once per frame
	void Update () {
	    if(Enemy.CurrentState == Enemy.myDeadState && !goldDropped)
        {
            GameObject temp = Instantiate(GoldPrefab, transform.position, Quaternion.identity) as GameObject;
            temp.GetComponent<GoldPickup>().GoldValue = (int)Random.Range(GoldDropRange.x, GoldDropRange.y);
            temp.AddComponent<Rigidbody>();//.AddForce;(new Vector3(0.0f, 3f, 0.0f));
            temp.GetComponent<Rigidbody>().AddForce(new Vector3(0.0f, -0.5f, 0.0f));
            goldDropped = true;
        }
	}

    void OnTriggerEnter(Collider other)
    {

    }
}
