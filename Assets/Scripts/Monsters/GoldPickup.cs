﻿using UnityEngine;
using System.Collections;

public class GoldPickup : MonoBehaviour {

    public int GoldValue;

    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            other.GetComponent<UnitStatManager>().ReceiveGold(GoldValue);
            Destroy(gameObject);
        }
    }
}
