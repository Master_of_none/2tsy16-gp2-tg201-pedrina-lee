﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDrop : MonoBehaviour {

    public List<GameObject> ItemDrops;
    public StatePatternEnemy Enemy;
    private bool itemDropped;

	// Use this for initialization
	void Start () {
        Enemy = GetComponent<StatePatternEnemy>();
        itemDropped = false;
	}
	
	// Update is called once per frame
	void Update () {
        ItemDrops.Find(x => x.name == name);

	    if(Enemy.CurrentState == Enemy.myDeadState && !itemDropped)
        {
            int randomIndex = Random.Range(0, ItemDrops.Count-1);

            GameObject temp = Instantiate(ItemDrops[randomIndex], transform.position, Quaternion.identity) as GameObject;
            itemDropped = true;
        }
	}
}
