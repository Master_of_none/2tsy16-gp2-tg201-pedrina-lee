﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(PlayerMovement))]

public class PlayerMoveAnimation : MonoBehaviour {

    public Animator MyAnimator;


    private Transform playerTransform;
    private PlayerMovement playerMovement;

    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
        playerTransform = transform;
    }

    // Use this for initialization
    void Start () {
        MyAnimator = GetComponent<Animator>();
	}

    void Update()
    {
        RunAnimation();
    }

    void RunAnimation()
    {
		MyAnimator.SetBool("Moving", Vector3.Distance(playerTransform.position, playerMovement.targetPosition) > playerMovement.StoppingDistance);
    }

    public void TriggerAction(string animParameter)
    {
        MyAnimator.SetTrigger(animParameter);
    }

    public void SetBool(string animParameter, bool state)
    {
        MyAnimator.SetBool(animParameter, state);
    }
}
