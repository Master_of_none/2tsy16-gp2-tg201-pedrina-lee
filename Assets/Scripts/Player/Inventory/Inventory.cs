﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Inventory : MonoBehaviour {

    public int ItemSlots;
    public string ItemDescription;
    public GameObject Description;
    public GameObject SlotPrefab;
    public GameObject SelectedSlot;
    public List<GameObject> Slots;
    public List<GameObject> Items;


	// Use this for initialization
	void Start () {
        Slots = new List<GameObject>();
        Items = new List<GameObject>();

        float x = -80.0f;
        float y = 100.0f;

        for(int i = 0; i < 5; i++)
        {
            for(int j = 0; j < 5; j++)
            {
                GameObject slot = (GameObject)Instantiate(SlotPrefab, transform.position, Quaternion.identity);
                slot.AddComponent<ItemCell>();
                slot.name = "Slot [" + (i + 1) + "] [" + (j + 1) + "]";
                slot.gameObject.transform.SetParent(transform, false);
                slot.GetComponent<RectTransform>().localPosition = new Vector3(x, y);
                x += (j == 4) ? -160 : 40;
            }
            y -= 40;
        }

        GetSlots();
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void AddItem(GameObject item)
    {
        if(Items.Contains(item) && (item.GetComponent(typeof(BaseItem)) as BaseItem).Stackable)
        {
            AddStack(item);
            return;
        }
        Items.Add(item);
        DisplayItem();
    }

    public void RemoveItem(BaseItem item)
    {
        Items.Remove(Items.Find(x => (x.GetComponent(typeof(BaseItem)) as BaseItem) == item));
    }

    public void UpdateDescription()
    {
        Description.GetComponentInChildren<Text>().text = ItemDescription;
    }

    private void DisplayItem()
    {
        List<GameObject> items = Items.Distinct().ToList<GameObject>();
        for(int i = 0; i < items.Count; i ++)
        {
            BaseItem item = items[i].GetComponent(typeof(BaseItem)) as BaseItem;
            for (int j = 0; j < Slots.Count; j++)
            {
                if (CheckSlot(item))
                    break;
                if (IsSlotEmpty(Slots[j]))
                {
                    Slots[j].GetComponent<ItemCell>().ItemAmount = 1;
                    Slots[j].GetComponent<ItemCell>().Item = item;
                    break;
                }
            }
        }
    }

    private bool CheckSlot(BaseItem item)
    {
        foreach (GameObject slot in Slots)
        {
            if (slot.GetComponent<ItemCell>().Item == item)
                return true;
        }
        return false;
    }

    private void AddStack(GameObject item)
    {
        BaseItem temp = item.GetComponent(typeof(BaseItem)) as BaseItem;
        foreach (GameObject slot in Slots)
        {
            if (slot.GetComponent<ItemCell>().Item == temp)
            {
                slot.GetComponent<ItemCell>().ItemAmount++;
                break;
            }
        }
    }

    private bool IsSlotEmpty(GameObject slot)
    {
        return slot.GetComponent<ItemCell>().Item == null;
    }

    private bool IsItemShown(GameObject slot, BaseItem item)
    {
        return slot.GetComponent<ItemCell>().Item == item;
    }

    private void GetSlots()
    {
        foreach(Transform slot in transform)
        {
            if (slot.name.Contains("Slot"))
            {
                Slots.Add(slot.gameObject);
            }
        }
    }

}
