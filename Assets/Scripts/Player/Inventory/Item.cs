﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item
{
    [SerializeField] public string ItemName;
    [SerializeField] public string ItemDescription;
    [SerializeField] public int ItemId;
    [SerializeField] public int ItemValue;
    [SerializeField] public bool Stackable;
    [SerializeField] public Sprite ItemSprite;
    [SerializeField] public GameObject ItemModel;
    [SerializeField] public ItemType Type;

    public enum ItemType
    {
        Consumable,
        Potion
    }

    public Item() { }

    public Item(string itemName, int itemId, int itemValue, bool stackable, Sprite sprite, ItemType type, string description)
    {
        ItemName = itemName;
        ItemId = itemId;
        ItemValue = itemValue;
        Stackable = stackable;
        ItemSprite = sprite;
        Type = type;
        ItemDescription = description;
    }
}
