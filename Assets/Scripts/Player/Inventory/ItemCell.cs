﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class ItemCell : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

    public int ItemAmount;
    public BaseItem Item;
    public Image ItemImage;
    public Text Amount;

	// Use this for initialization
	void Start () {
        ItemImage = transform.FindChild("Item").GetComponent<Image>();
        Amount = transform.FindChild("Amount").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
	    if(Item != null)
        {
            ItemImage.sprite = Item.ItemSprite;
            ItemImage.color = Color.white;
        }
        else
        {
            ItemImage.sprite = null;
            ItemImage.color = Color.clear;
        }

        Amount.text = (ItemAmount == 0) ? "" : ItemAmount.ToString();
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        GetComponentInParent<Inventory>().SelectedSlot = gameObject;
        if (Item != null)
        {
            GetComponentInParent<Inventory>().ItemDescription = Item.Description;
            GetComponentInParent<Inventory>().UpdateDescription();
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponentInParent<Inventory>().SelectedSlot = null;
        if (Item != null)
        {
            GetComponentInParent<Inventory>().ItemDescription = "";
            GetComponentInParent<Inventory>().UpdateDescription();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Item != null)
        {
            ItemAmount--;
            Item.Use();
            if (ItemAmount <= 0)
            {
                GetComponentInParent<Inventory>().RemoveItem(Item);
                Item = null;
            }

        }
    }
}
