﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public class ItemDatabaseEditor : EditorWindow {

    private enum State
    {
        Blank,
        Edit,
        Add
    }
    private State state;
    private int selectedItem;

    private string newItemName;
    private string newItemDesc;
    private int newItemId;
    private int newItemValue;
    private bool isStackable;
    private Item.ItemType newItemType;
    private Sprite newItemSprite;
    private GameObject newItemModel;

    private const string DATABASE_PATH = @"Assets/Database/itemDB.asset";
    private ItemDatabase items;
    private Vector2 scrollPos;

    [MenuItem("Database/Item Database %#w")]
    public static void Init()
    {
        ItemDatabaseEditor window = (ItemDatabaseEditor)EditorWindow.GetWindow(typeof(ItemDatabaseEditor), true);
        window.minSize = new Vector2(800, 400);
        window.Show();
    }

    void OnEnable()
    {
        if (items == null)
            LoadDatabase();
        state = State.Blank;
    }

    void OnGUI()
    {
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        DisplayListArea(); DisplayMainArea();
        EditorGUILayout.EndHorizontal();
    }

    void LoadDatabase()
    {
        items = (ItemDatabase)AssetDatabase.LoadAssetAtPath(DATABASE_PATH, typeof(ItemDatabase));
        if (items == null)
            CreateDatabase();
    }

    void CreateDatabase()
    {
        items = ScriptableObject.CreateInstance<ItemDatabase>();
        ItemDatabase.Instance = items;
        AssetDatabase.CreateAsset(items, DATABASE_PATH);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    void DisplayListArea()
    {
        EditorGUILayout.BeginVertical(GUILayout.Width(250));
        EditorGUILayout.Space();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, "box", GUILayout.ExpandHeight(true));
        for (int cnt = 0; cnt < items.Count; cnt++)
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("-", GUILayout.Width(25)))
            {
                items.RemoveAt(cnt); items.SortAlphabeticallyAtoZ();
                EditorUtility.SetDirty(items);
                state = State.Blank;
                return;
            }
            if (GUILayout.Button(items.GetItem(cnt).ItemName, "box", GUILayout.ExpandWidth(true)))
            {
                selectedItem = cnt;
                state = State.Edit;
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndScrollView();
        EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
        EditorGUILayout.LabelField("Items: " + items.Count, GUILayout.Width(100));
        if (GUILayout.Button("New Item"))
        {
            newItemName = null;
            newItemDesc = "";
            newItemId = 0;
            newItemValue = 0;
            isStackable = false;
            newItemSprite = null;
            newItemModel = null;
            state = State.Add;
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        EditorGUILayout.EndVertical();
    }

    void DisplayMainArea()
    {
        EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(true));
        EditorGUILayout.Space();
        switch (state)
        {
            case State.Add:
                DisplayAddMainArea();
                break;

            case State.Edit:
                DisplayEditMainArea();
                break;

            default:
                DisplayBlankMainArea();
                break;
        }
        EditorGUILayout.Space();
        EditorGUILayout.EndVertical();
    }

    void DisplayBlankMainArea()
    {
        EditorGUILayout.LabelField("There are 3 things that can be displayed here.\n" + "1) Item info for editing\n" + "2) Black fields for adding a new item\n" + "3) Blank Area", GUILayout.ExpandHeight(true));
    }

    void DisplayEditMainArea()
    {
        items.GetItem(selectedItem).ItemName     = EditorGUILayout.TextField(new GUIContent("Name: "), items.GetItem(selectedItem).ItemName);
        items.GetItem(selectedItem).Type         = (Item.ItemType)EditorGUILayout.EnumPopup("Item Type: ", newItemType);
        items.GetItem(selectedItem).ItemId       = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Item ID: "), newItemId.ToString()));
        items.GetItem(selectedItem).ItemValue    = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Item Value: "), newItemValue.ToString()));
        items.GetItem(selectedItem).Stackable    = Convert.ToBoolean(EditorGUILayout.Toggle(new GUIContent("Stackable: "), isStackable));
        items.GetItem(selectedItem).ItemSprite   = (Sprite)EditorGUILayout.ObjectField("Sprite: ", newItemSprite, typeof(Sprite), true);

        EditorGUILayout.Space();
        if (GUILayout.Button("Done", GUILayout.Width(100)))
        {
            items.SortAlphabeticallyAtoZ();
            EditorUtility.SetDirty(items);
            state = State.Blank;
        }
    }

    void DisplayAddMainArea()
    {
        newItemName     = EditorGUILayout.TextField(new GUIContent("Name: "), newItemName);
        newItemDesc     = EditorGUILayout.TextField(new GUIContent("Description: "), newItemDesc);
        newItemType     = (Item.ItemType)EditorGUILayout.EnumPopup("Item Type: ", newItemType);
        newItemId       = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Item ID: "), newItemId.ToString()));
        newItemValue    = Convert.ToInt32(EditorGUILayout.TextField(new GUIContent("Item Value: "), newItemValue.ToString()));
        isStackable     = Convert.ToBoolean(EditorGUILayout.Toggle(new GUIContent("Stackable: "), isStackable));
        newItemSprite   = (Sprite)EditorGUILayout.ObjectField("Sprite: ", newItemSprite, typeof(Sprite), true);
        
        
        EditorGUILayout.Space();
        if (GUILayout.Button("Done", GUILayout.Width(100)))
        {
            items.Add(new Item(newItemName, newItemId, newItemValue, isStackable, newItemSprite, newItemType, newItemDesc));
            items.SortAlphabeticallyAtoZ();
            newItemName = string.Empty;
            //newWeaponDamage = 0;
            EditorUtility.SetDirty(items);
            state = State.Blank;
        }
    }
}
