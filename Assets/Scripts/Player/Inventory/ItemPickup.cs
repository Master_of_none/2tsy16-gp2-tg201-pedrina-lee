﻿using UnityEngine;
using System.Collections;

public class ItemPickup : MonoBehaviour {
    
    public BaseItem ItemData;
    public NewItemDatabase Database;

    public void Start()
    {
        ItemData = GetComponent(typeof(BaseItem)) as BaseItem;
        Database = GameObject.FindGameObjectWithTag("ItemDatabase").GetComponent<NewItemDatabase>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            other.GetComponent<PlayerStateBehaviour>().PlayerInventory.GetComponent<Inventory>().AddItem(Database.GetItem(gameObject));
            Destroy(gameObject);
        }
    }
}
