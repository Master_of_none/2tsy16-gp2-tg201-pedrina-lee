﻿using UnityEngine;
using System.Collections;

public class BaseItem : MonoBehaviour {
    
    public int ItemID;
    public bool Stackable;
    public string Name;
    public string Description; 
    public int Cost;
    public Sprite ItemSprite;

    public virtual void Use()
    {
        
    }

}
