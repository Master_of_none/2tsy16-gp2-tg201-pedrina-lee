﻿using UnityEngine;
using System.Collections;

public class BuffingItem : BaseItem
{

    public float BuffDuration;
    public int BuffValue;
    public string BuffStat;
    private BaseStat statBuffed;
    private UnitStatManager stats;
    private GameObject player;

    // Use this for initialization
    void Start()
    {

    }

    IEnumerator AddStat()
    {
        statBuffed.BonusValue += BuffValue;
        yield return new WaitForSeconds(BuffDuration);
        statBuffed.BonusValue -= BuffValue;
    }

    public override void Use()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        stats = player.GetComponent<UnitStatManager>();
        statBuffed = stats.FindBaseStat(BuffStat);
        if (statBuffed != null)
        {
            statBuffed.BonusValue += BuffValue;
        }
        else if (statBuffed == null)
        {
            print("Stat does not exist");
        }
    }
}
