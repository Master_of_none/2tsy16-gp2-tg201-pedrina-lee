﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class HealingItem : BaseItem {

    public Type HealingType;
    public int RecoveryValue;
    public GameObject Target;


    public enum Type
    {
        HealthPotion,
        ManaPotion
    }

    public HealingItem() { }
    
    public HealingItem(HealingItem clone)
    {
        ItemID = clone.ItemID;
        Name = clone.Name;
        Description = clone.Description;
        Cost = clone.Cost;
        ItemSprite = clone.ItemSprite;
        RecoveryValue = clone.RecoveryValue;
        Target = clone.Target;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    public override void Use()
    {
        Target = GameObject.FindGameObjectWithTag("Player") ;
        switch(HealingType)
        {
            case Type.HealthPotion:
                Target.GetComponent<UnitStatManager>().CurrentHealth += RecoveryValue;
                break;
            case Type.ManaPotion:
                Target.GetComponent<UnitStatManager>().CurrentMana += RecoveryValue;
                break;
        }
    }
}
