﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NewItemDatabase : MonoBehaviour {

    public List<GameObject> Items;

    public void AddItem(GameObject newItem)
    {
        Items.Add(newItem);
    }

    public int Count
    {
        get { return Items.Count; }
    }

    public GameObject GetItem(int index)
    {
        return Items[index];
    }

    public GameObject GetItem(string name)
    {
        return Items.Find(x => (x.GetComponent(typeof(BaseItem)) as BaseItem).Name.ToLower() == name.ToLower());
    }

    public GameObject GetItem(GameObject clone)
    {
        foreach(GameObject item in Items)
        {
            if ((item.GetComponent(typeof(BaseItem)) as BaseItem).ItemID == (clone.GetComponent(typeof(BaseItem)) as BaseItem).ItemID)
                return item;
        }
        Debug.Log("Can't find item");
        return null;
    }

    public void RemoveAt(int index)
    {
        Items.RemoveAt(index);
    }
}
