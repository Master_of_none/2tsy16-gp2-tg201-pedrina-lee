﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]

public class PlayerMovement : MonoBehaviour {

    public GameObject Cursor;
    public CharacterController PlayerController;
    public float Speed;
    public float StoppingDistance;
    public float RotateSpeed;
    public float Gravity;
	public LayerMask Grounded;

	[HideInInspector]   
	public Vector3 targetPosition;

	// Use this for initialization
	void Start ()
    {
        PlayerController = GetComponent<CharacterController>();
        targetPosition = transform.position;
	}

    // Update is called once per frame
    void Update()
    {
        GetPosition();
        Move();
    }

    void Move()
    {
        Vector3 moveDirection = targetPosition - transform.localPosition;
        moveDirection.Normalize();
        moveDirection.y = -Gravity;

        if (Vector3.Distance(transform.position, targetPosition) > StoppingDistance)
        {
            Quaternion LookAt = Quaternion.LookRotation(targetPosition - transform.position);
            LookAt.x = 0;
            LookAt.z = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, LookAt, RotateSpeed * Time.deltaTime);
            
        }
        else
        {
			Cursor.SetActive (false);
            moveDirection.Set(0, -Gravity, 0);
        }

        PlayerController.Move(moveDirection * Speed * Time.deltaTime);
    }

    void GetPosition()
    {
		//Add Idle Animation and Run Animation
		
        if (Input.GetMouseButtonDown(0))
        {
			RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray, out hit, Mathf.Infinity, Grounded))
			{
				targetPosition = hit.point;
			}
        }
    }
}
