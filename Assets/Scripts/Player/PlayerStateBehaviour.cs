﻿using UnityEngine;
using System.Collections;

public class PlayerStateBehaviour : MonoBehaviour {

    public float AttackSpeed;
    public float AttackRange;
    public float MoveSpeed;
    public float RotateSpeed;
    public float Gravity;
    public float StoppingDistance;
    public bool BladeDrawn;
    public Vector3 TargetPosition;
    public GameObject MoveMarker;
    public GameObject AttackMarker;
    public GameObject Target;
    public GameObject PlayerInventory;
    public CharacterController PlayerController;
    public LayerMask Mask;
    public Animator Anim;
    public Animator TestAnim;
    public State CurrentState;
    public TestState CurrState;
    //public Skill CurrentSkill;

    private GameObject spawnedCursor;
    private float lastAttack;
    private bool canMove;
    private int pointMode = 1;

    private const int ENEMYSELECTED = 0;
    private const int GROUNDSELECTED = 1;

    public enum State
    {
        Idle,
        Move,
        Attack,
        Chase,
        IdleCombat,
        Combat,
        Skill,
        Dead
    }

    public enum TestState
    {
        Idle,
        Move,
        Chase,
        Attack,
        Skill,
        Dead
    }
    public bool CanMove
    {
        get { return canMove; }
        set { canMove = value; }
    }

    // Use this for initialization
    void Awake () {
        CurrentState = State.Idle;
        CurrState = TestState.Idle;
        TestAnim = GetComponent<Animator>();
        PlayerController = GetComponent<CharacterController>();
        canMove = true;
        MoveMarker.SetActive(false);
        AttackMarker.SetActive(false);
	}
	
	// Update is called once per frame
	public void Update () {
        Raycast();
        TestUpdateState();
        //StartCoroutine(UpdateState());
        //StopCoroutine(UpdateState());
        UpdateAnim();
	}

    void UpdateAnim()
    {
        TestAnim.SetInteger("State", (int)CurrState);
    }

    public void ToggleMovement()
    {
        canMove = !canMove;
        UpdateAnim();
    }


    private void Raycast()
    {
        if(Input.GetMouseButtonDown(0) && canMove)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f, Mask))
            {
                int layerHit = hit.collider.gameObject.layer;
                if (layerHit == LayerMask.NameToLayer("Ground"))
                {
                    Target = null;
                    BladeDrawn = false;
                    //Anim.SetTrigger("PutBlade");

                    AttackMarker.SetActive(false);
                    AttackMarker.transform.SetParent(transform);
                    MoveMarker.SetActive(true);
                    MoveMarker.transform.position = hit.point + new Vector3(0.0f, 0.5f);
                    CurrState = TestState.Move;
                }
                else if(layerHit == LayerMask.NameToLayer("Enemy"))
                {
                    Target = hit.collider.gameObject;
                    BladeDrawn = true;
                    //Anim.SetTrigger("DrawBlade");

                    MoveMarker.SetActive(false);
                    AttackMarker.SetActive(true);
                    AttackMarker.transform.position = hit.transform.position + new Vector3(0.0f, 0.1f);
                    AttackMarker.transform.SetParent(hit.transform);
                    CurrState = TestState.Chase;
                }
            }
            if(hit.point != null)
                TargetPosition = hit.point;

            //Anim.SetBool("BladeDrawn", BladeDrawn);
        }
    }

    public void TestUpdateState()
    {
        switch(CurrState)
        {
            case TestState.Move:
                Move(TargetPosition);
                break;
            case TestState.Skill:
                UpdateAnim();
                if (TestAnim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0f && TestAnim.GetCurrentAnimatorStateInfo(0).IsName("Skill"))
                {
                    print("if1");
                    CurrState = TestState.Idle;
                }
                break;
            case TestState.Chase:
                Move(Target.transform.position);
                if (Vector3.Distance(transform.position, Target.transform.position) <= AttackRange)
                {
                    CurrState = TestState.Attack;
                }
                break;
            case TestState.Attack:
                if (Target != null)
                {
                    Quaternion LookAt = Quaternion.LookRotation(Target.transform.position - transform.position);
                    LookAt.x = 0;
                    LookAt.z = 0;
                    transform.rotation = Quaternion.Slerp(transform.rotation, LookAt, RotateSpeed * Time.deltaTime);
                    if (Vector3.Distance(transform.position, Target.transform.position) > AttackRange)
                    {
                        CurrState = TestState.Chase;
                    }
                }
                else if (Target == null)
                {
                    CurrState = TestState.Idle;
                }
                break;
            case TestState.Dead:
                Destroy(this.gameObject, 5);
                break;
        }        
        //print(TestAnim.GetCurrentAnimatorStateInfo(0).normalizedTime);
    }

    private IEnumerator UpdateState()
    {
        switch (CurrentState)
        {
            case State.Idle:
                if (Vector3.Distance(transform.position, TargetPosition) > StoppingDistance && Input.GetMouseButtonDown(0))
                {
                    CurrentState = State.Move;
                }

                if (EnemySelected())
                {
                    CurrentState = State.Combat;
                }

                break;
            case State.Move:
                Move(TargetPosition);

                if (EnemySelected())
                {
                    CurrentState = State.Combat;
                }

                break;
            case State.IdleCombat:

                break;
            case State.Combat:
                
                yield return new WaitForSeconds(1);
                CurrentState = State.Chase;
                break;
            case State.Chase:
                if (Target != null)
                {
                    if (Vector3.Distance(transform.position, Target.transform.position) <= AttackRange)
                    {
                        Anim.SetBool("Moving", false);
                        CurrentState = State.Attack;
                    }
                    else
                    {
                        Move(Target.transform.position);
                    }
                }
                else
                {
                    CurrentState = State.Move;
                }
                break;
            case State.Attack:

                if (Target != null)
                {
                    Quaternion LookAt = Quaternion.LookRotation(Target.transform.position - transform.position);
                    LookAt.x = 0;
                    LookAt.z = 0;

                    transform.rotation = Quaternion.Slerp(transform.rotation, LookAt, RotateSpeed * Time.deltaTime);
                }
                else if(Target == null)
                {
                    
                    break;
                }
                if (Vector3.Distance(transform.position, Target.transform.position) > AttackRange)
                {
                    CurrentState = State.Chase;
                }
                if (Time.time > lastAttack + AttackSpeed)
                {
                    lastAttack = Time.time;
                    Anim.SetTrigger("Attack");
                }

                break;

            case State.Dead:
                tag = "Untagged";
                Anim.SetBool("Dead", true);
                break;
        }
        yield return null;
    }

    private bool EnemySelected()
    {
        return Target != null;
    }

    private void Move(Vector3 position)
    {
        Vector3 moveDirection = position - transform.localPosition;
        float distance = Vector3.Distance(transform.position, TargetPosition);

        moveDirection.Normalize();
        moveDirection.y = -Gravity;

        if (distance > StoppingDistance)
        {
            Quaternion LookAt = Quaternion.LookRotation(position - transform.position);
            LookAt.x = 0;
            LookAt.z = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, LookAt, RotateSpeed * Time.deltaTime);
            PlayerController.Move(moveDirection * MoveSpeed * Time.deltaTime);
            //Anim.SetBool("Moving", true);
        }
        else if (distance <= StoppingDistance)
        {
            //Anim.SetBool("Moving", false);
            MoveMarker.SetActive(false);
            moveDirection.Set(0, -Gravity, 0);
            CurrState = TestState.Idle;
            CurrentState = State.Idle;
        }

    }

    public void AreaAttack()
    {
        Collider[] enemiesHit = Physics.OverlapSphere(transform.position, 5);
        foreach (Collider enemy in enemiesHit)
        {
            if (enemy.tag == "Enemy")
                //enemy.GetComponent<EnemyStatManager>().Damage(GetComponent<PlayerStatManager>().DerivedStats[2].TotalValue);
                enemy.GetComponent<UnitStatManager>().Damage(GetComponent<UnitStatManager>().DerivedStats[2].TotalValue);
        }
    }
}

