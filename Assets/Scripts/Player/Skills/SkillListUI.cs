﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;

public class SkillListUI : MonoBehaviour {

    public List<GameObject> Skills;
    public GameObject SkillPrefab;

	// Use this for initialization
	void Start () {
        InitializeSkills();   	
	}
	
	// Update is called once per frame
	void Update () {

	}

    void InitializeSkills()
    {

        for(int i = 0; i < Skills.Count; i++)
        {
            GameObject skillTemp = Instantiate(SkillPrefab) as GameObject;
            skillTemp.transform.parent = transform;
            skillTemp.transform.localScale = transform.localScale;
            skillTemp.transform.position += transform.position + transform.localPosition + (new Vector3(0.0f, -32.5f, 0.0f) * i);
            /*
            skillTemp.name = Skills[i].name;
            skillTemp.transform.FindChild("SkillSprite").GetComponent<Image>().sprite = skill.SkillSprite;
            skillTemp.transform.FindChild("SkillName").GetComponent<Text>().text = skill.SkillName;
            skillTemp.transform.FindChild("SkillDescription").GetComponent<Text>().text = skill.Description;
            skillTemp.transform.FindChild("SkillMPCost").GetComponent<Text>().text = "MP: " + skill.MPCost.ToString();
            skillTemp.transform.FindChild("SkillCooldown").GetComponent<Text>().text = "CD: " + skill.Cooldown.ToString() + " s"    ;*/

            Skills.Add(skillTemp);
        }
    }
}
