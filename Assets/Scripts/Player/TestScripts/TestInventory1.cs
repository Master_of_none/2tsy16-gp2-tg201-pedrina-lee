﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class TestInventory1 : MonoBehaviour {

    public GameObject SelectedItem;
    public List<Item> ItemInventory;
    public List<GameObject> ItemSlots;

    public GameObject player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        ItemInventory = player.GetComponent<TestPlayerInventory>().ItemInventory;
        ItemSlots = new List<GameObject>();
        GetAllSlots();
    }


    // Update is called once per frame
    void Update()
    {
        print(player.name);
        DisplayItems();
        
    }
    private void DisplayItems()
    {
        for (int j = 0; j < ItemInventory.Count; j++)
        {
            //ItemSlots[i].GetComponent<InvCellController>().Item = ItemInventory[j];
            if (ItemSlots[j].GetComponent<InventoryCell>().Item == null)
            {
                ItemSlots[j].GetComponent<InventoryCell>().Item = ItemInventory[j];
                break;
            }
        }
    }

    private void GetAllSlots()
    {
        foreach (Transform child in transform)
        {
            ItemSlots.Add(child.gameObject);
        }
    }
}
