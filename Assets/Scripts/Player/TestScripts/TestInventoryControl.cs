﻿using UnityEngine;
using System.Collections;

public class TestInventoryControl : MonoBehaviour {
    private GameObject inventory;
	// Use this for initialization
	void Start () {
        inventory = GameObject.FindGameObjectWithTag("Inventory");
	}
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetKeyDown(KeyCode.I)) && (inventory.activeSelf))
            inventory.SetActive(false);
        else if ((Input.GetKeyDown(KeyCode.I)) && (!inventory.activeSelf))
            inventory.SetActive(true);
    }
}
