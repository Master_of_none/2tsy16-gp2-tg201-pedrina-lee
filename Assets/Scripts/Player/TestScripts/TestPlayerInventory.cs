﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestPlayerInventory : MonoBehaviour {

    public List<Item> ItemInventory;

    public void AddItem(Item item)
    {
        ItemInventory.Add(item);
    }

    public void  Update()
    {
        foreach(Item item in ItemInventory)
        {
            print(item.ItemName);
        }
    }
}
