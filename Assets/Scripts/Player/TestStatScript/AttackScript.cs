﻿using UnityEngine;
using System.Collections;

public class AttackScript : MonoBehaviour {

    UnitStatManager stats;
    PlayerStateBehaviour player;

    void Start()
    {
        stats = GetComponent<UnitStatManager>();
        player = GetComponent<PlayerStateBehaviour>();
    }

    public void StartEffect()
    {
        player.ToggleMovement();
        player.transform.Find("Effects").transform.Find("SkillEffects").gameObject.SetActive(true);
    }

    public void StopEffect()
    {
        player.ToggleMovement();
        player.transform.Find("Effects").transform.Find("SkillEffects").gameObject.SetActive(false);
    }

    public void AttackStartEffect()
    {
        player.transform.Find("Effects").transform.Find("AttackEffects").gameObject.SetActive(true);
    }

    public void AttackStopEffect()
    {
        player.transform.Find("Effects").transform.Find("AttackEffects").gameObject.SetActive(false);
    }

    public void DamageTo()
    {
        if(player.Target != null)
            player.Target.GetComponent<UnitStatManager>().Damage((int)stats.DerivedStats[0].TotalValue);
        if (player.Target.GetComponent<UnitStatManager>().CurrentHealth <= 0)
        {
            player.Target = null;
            player.CurrentState = PlayerStateBehaviour.State.Idle;
        }
    }
}
