﻿using UnityEngine;
using System.Collections;

public class EnemyAttackScript : MonoBehaviour {

    UnitStatManager stats;
    StatePatternEnemy enemy;

    void Start()
    {
        stats = GetComponent<UnitStatManager>();
        enemy = GetComponent<StatePatternEnemy>();
    }

    public void DamageTo()
    {
        enemy.Target.GetComponent<UnitStatManager>().Damage(stats.DerivedStats[0].TotalValue);
        if(enemy.Target.GetComponent<UnitStatManager>().CurrentHealth <= 0)
        {
            enemy.CurrentState = enemy.myIdleState;
            enemy.Target = null;
        }
    }
}
