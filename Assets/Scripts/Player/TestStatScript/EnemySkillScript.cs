﻿using UnityEngine;
using System.Collections;

public class EnemySkillScript : MonoBehaviour {

    UnitStatManager stats;
    void Awake()
    {
        stats = GetComponent<UnitStatManager>();
    }
    public void DamageTo()
    {
        BroadcastMessage("Use",stats);
    }
}
