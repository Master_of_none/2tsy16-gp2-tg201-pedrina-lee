﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TestPlayerStatDisplay : MonoBehaviour {

    //public Text Stats;
    public Image Stats;
    public Text Level;
    public Text Exp;
    public Text Health;
    public Text Mana;
    public Text ManaRegen;
    public Text Attack;
    public Text MagicAttack;
    public Text Points;
    public Text STR;
    public Text VIT;
    public Text SPR;
    public Text INT;
    public Text Gold;
    public Slider ExpBar;
    
     
    private UnitStatManager stats;

    void Start()
    {
        stats = GetComponent<UnitStatManager>();
    }

    void Update()
    {
        SetStatDisplay();
    }

    public void SetStatDisplay()
    {
        print(stats.Level);
        Level.text = "Level: " + stats.Level;
        Exp.text = "EXP: " +  stats.CurrentExp;
        Health.text = "Health: " + stats.CurrentHealth + "/" + stats.MaxHealth;
        Mana.text = "Mana: " + ((int)stats.CurrentMana) + " / " + stats.MaxMana;
        ManaRegen.text = "Mana Regen: " + stats.DerivedStats[3].TotalValue;
        Attack.text = "ATK: " + stats.DerivedStats[0].TotalValue;
        MagicAttack.text = "MATK: " + stats.DerivedStats[4].TotalValue;
        Points.text = "StatPoints: " + stats.StatPoints;
        STR.text = "STR: " + stats.BaseStats[0].TotalValue;
        VIT.text = "VIT: " + stats.BaseStats[1].TotalValue;
        INT.text = "INT: " + stats.BaseStats[2].TotalValue;
        SPR.text = "SPR: " + stats.BaseStats[3].TotalValue;
        Gold.text = "Gold: " + stats.Gold;
        if (stats.CurrentHealth <= 0)
            stats.CurrentHealth = 0;

        if (stats.CurrentExp >= stats.MaxExp)
            stats.CurrentExp -= stats.MaxExp;
        
        ExpBar.value = stats.CurrentExp / stats.MaxExp;
    }


}
