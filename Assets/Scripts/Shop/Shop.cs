﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Shop : MonoBehaviour {

    public List<GameObject> Slots;
    public List<GameObject> Items;
    public GameObject SlotPrefab;
    public GameObject ShopTabPrefab;
    public GameObject ItemDatabase;
    private GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        Slots = new List<GameObject>();
        Items = new List<GameObject>();
        
        float y = 106.0f;

        for (int i = 0; i < 5; i++)
        {
            GameObject slot = (GameObject)Instantiate(SlotPrefab, transform.position, Quaternion.identity);
            slot.AddComponent<ShopCell>();
            slot.name = "Slot [" + (i + 1) + "]";
            slot.gameObject.transform.SetParent(transform, false);
            slot.GetComponent<RectTransform>().localPosition = new Vector3(0.0f, y);
            y -= (i % 5 == 0 && i != 0) ? -160 : 60;
        } 
        GetSlots();
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    public void SetupShop(List<GameObject> itemsToSell)
    {
        Items = itemsToSell;
        DisplayItem();
    }

    public void BuyItem(BaseItem itemToBuy)
    {
        if (player.GetComponent<UnitStatManager>().Gold >= itemToBuy.Cost && itemToBuy != null)
        {
            player.GetComponent<PlayerStateBehaviour>().PlayerInventory.GetComponent<Inventory>().AddItem(ItemDatabase.GetComponent<NewItemDatabase>().GetItem(itemToBuy.Name));
            player.GetComponent<UnitStatManager>().Gold -= itemToBuy.Cost;
        }
        else
        {
            Debug.Log("Insufficient Gold");
        }
    }

    private void DisplayItem()
    {
        List<GameObject> items = Items.Distinct().ToList<GameObject>();
        for (int i = 0; i < items.Count; i++)
        {
            BaseItem item = items[i].GetComponent(typeof(BaseItem)) as BaseItem;
            for (int j = 0; j < Slots.Count; j++)
            {
                if (ItemIsInInventoryUI(item))
                    break;
                if (IsSlotEmpty(Slots[j]))
                {
                    Slots[j].GetComponent<ShopCell>().ItemAmount = 1;
                    Slots[j].GetComponent<ShopCell>().Item = item;
                    break;
                }
            }
        }
    }
    private bool ItemIsInInventoryUI(BaseItem item)
    {
        foreach (GameObject slot in Slots)
        {
            if (slot.GetComponent<ShopCell>().Item == item)
                return true;
        }
        return false;
    }

    private bool IsSlotEmpty(GameObject slot)
    {
        return slot.GetComponent<ShopCell>().Item == null;
    }

    private void GetSlots()
    {
        foreach(Transform slot in transform)
        {
            if(slot.name.Contains("Slot"))
            {
                Slots.Add(slot.gameObject);
            }
        }
    }
}
