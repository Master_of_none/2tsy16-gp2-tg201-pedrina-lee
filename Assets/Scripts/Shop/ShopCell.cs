﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class ShopCell : MonoBehaviour, IPointerClickHandler {
    public int ItemAmount;
    public BaseItem Item;
    public Image ItemImage;
    public Text ItemPrice;
    public Text ItemName;
    public Text Description;

    // Use this for initialization
    void Start()
    {
        ItemName = transform.FindChild("ItemName").GetComponent<Text>();
        Description = transform.FindChild("Description").GetComponent<Text>();
        ItemImage = transform.FindChild("ItemSprite").GetComponent<Image>();
        ItemPrice = transform.FindChild("ItemPrice").GetComponent<Text>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Item != null)
        {
            ItemName.text = Item.Name;
            Description.text = Item.Description;
            ItemImage.sprite = Item.ItemSprite;
            ItemImage.color = Color.white;
            
        }
        else
        {
            ItemImage.sprite = null;
            ItemImage.color = Color.clear;
        }

        ItemPrice.text = (ItemAmount == 0) ? "$ 0" : "$ " + Item.Cost.ToString();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GetComponentInParent<Shop>().BuyItem(Item);
    }
}
