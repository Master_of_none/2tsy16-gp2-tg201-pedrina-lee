﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShopInteraction : MonoBehaviour
{

    public float InteractionDistance;
    public Text Prompt;
    public Animator Anim;
    public List<GameObject> ItemsForSale;
    public GameObject ShopUI;
    private GameObject player;
    private State currentState;

    public enum State
    {
        Idle,
        Talk
    }

    // Use this for initialization
    void Start()
    {
        currentState = State.Idle;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Interact()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            ShopUI.SetActive(!ShopUI.activeSelf);
            ShopUI.GetComponent<Shop>().SetupShop(ItemsForSale);
            currentState = (ShopUI.activeSelf) ? State.Talk : State.Idle;
            if (!ShopUI.activeSelf && !player.GetComponent<PlayerStateBehaviour>().CanMove)
                player.GetComponent<PlayerStateBehaviour>().ToggleMovement();
        }
    }

    void FacePlayer(GameObject Player)
    {
        Quaternion LookAt = Quaternion.LookRotation(Player.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, LookAt, Time.deltaTime * 5);
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Prompt.text = "Press \"E\" to Interact";
            Interact();
            FacePlayer(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Prompt.text = "";
            currentState = State.Idle;
            if (ShopUI.activeSelf)
                ShopUI.SetActive(false);
        }
    }
}
