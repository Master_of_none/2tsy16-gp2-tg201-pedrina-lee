﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopTab : MonoBehaviour {

    public List<GameObject> Tabs;

	// Use this for initialization
	void Start () {
        GetSlots();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GetSlots()
    {
        foreach (Transform slot in transform)
        {
            if(slot.name.Contains("ShopSlot"))
            {
                Tabs.Add(slot.gameObject);
            }
        }
    }

}
