﻿using UnityEngine;
using System.Collections;

public class Anim : MonoBehaviour {

    private PlayerStateBehaviour player;
    public PlayerStateBehaviour.TestState state;
    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateBehaviour>();
    }

    public void PlayAnim()
    {
        player.CurrState = state;
    }
}
