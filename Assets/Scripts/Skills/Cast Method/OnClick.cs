﻿using UnityEngine;
using System.Collections;

public class OnClick : MonoBehaviour {

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SendMessage("OnCast");
            SendMessage("PlayAnim");
        }
    }
}
