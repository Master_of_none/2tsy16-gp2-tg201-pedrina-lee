﻿using UnityEngine;
using System.Collections;

public class OnKeyPress : MonoBehaviour {

    public KeyCode key;
    public int delay;

    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            StartCoroutine(Queue());
            
        }
    }

    IEnumerator Queue()
    {
        SendMessage("PlayAnim");
        yield return new WaitForSeconds(delay);

        SendMessage("OnCast");
        yield return new WaitForSeconds(delay);
    }
}
