﻿using UnityEngine;
using System.Collections;

public class Buff : MonoBehaviour {
    
    public int value;
    private UnitStatManager myStats;
    private GameObject player;
    private float statholder1;
    private float statholder2;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        myStats = player.GetComponent<UnitStatManager>();
    }

    void Start()
    {
        statholder1 = myStats.BaseStats[1].BonusValue;
        statholder2 = myStats.BaseStats[2].BonusValue;

        myStats.BaseStats[1].BonusValue = value;
        myStats.BaseStats[2].BonusValue = value;
    }

    void OnDestroy()
    {
        myStats.BaseStats[1].BonusValue = statholder1;
        myStats.BaseStats[2].BonusValue = statholder2;
    }
}
