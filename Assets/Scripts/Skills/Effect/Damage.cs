﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Damage : MonoBehaviour {

    private int damage;

    public void SetValues(int value)
    {
        damage = value;
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            //Debug.Log("hit");
            other.GetComponent<UnitStatManager>().Damage(damage);
        }
    }
}
