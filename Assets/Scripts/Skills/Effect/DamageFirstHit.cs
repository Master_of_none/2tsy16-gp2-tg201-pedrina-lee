﻿using UnityEngine;
using System.Collections;

public class DamageFirstHit : MonoBehaviour {

    private int damage;

    public void SetValues(int value)
    {
        damage = value;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<UnitStatManager>().Damage(damage);
            Destroy(gameObject);
        }
    }
}
