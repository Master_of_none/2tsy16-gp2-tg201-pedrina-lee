﻿using UnityEngine;
using System.Collections;

public class TempEnemyHit : MonoBehaviour {

    private int damage;

    public void SetValues(int value)
    {
        damage = value;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<UnitStatManager>().Damage(damage);
            Destroy(gameObject);
        }
    }
}
