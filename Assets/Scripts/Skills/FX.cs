﻿using UnityEngine;
using System.Collections;

public class FX : MonoBehaviour {

    public GameObject effects;

    public void PlayFX()
    {
        var obj = Instantiate(effects) as GameObject;
        var holder = obj.transform.rotation;
        obj.transform.parent = gameObject.transform;
        obj.transform.position = gameObject.transform.position;
        
        if (obj.transform.rotation != Quaternion.Euler(holder.x, holder.y, holder.z))
            Quaternion.Euler(holder.x, holder.y, holder.z);   
    }
}
