﻿using UnityEngine;
using System.Collections;

public class RestoreOnSelf : MonoBehaviour {

    public int value;
    private UnitStatManager myStats;

    void Awake()
    {
        myStats = GetComponentInParent<UnitStatManager>();
    }

    void OnCast(UnitStatManager stats)
    {
        myStats = GetComponentInParent<UnitStatManager>();
        myStats.CurrentHealth+=value;
        if (myStats.CurrentHealth > myStats.MaxHealth)
            myStats.CurrentHealth = myStats.MaxHealth;
    }
}
