﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

    public GameObject prefab;
    private Transform origin;
    public float radius;

    void Start()
    {
        origin = GetComponentInParent<Transform>();
    }

    void OnCast(UnitStatManager stats)
    {
        var obj = Instantiate(prefab) as GameObject;
        obj.SendMessage("SetValues", stats.mAttack * 4);
        obj.transform.position = origin.position;
    }
}
