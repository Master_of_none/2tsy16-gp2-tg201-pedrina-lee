﻿using UnityEngine;
using System.Collections;

public class SpawnWithVelocity : MonoBehaviour {

    public GameObject prefab;
    private Transform origin;
    public float force;

    void Start()
    {
        origin = GetComponentInParent<Transform>();
    }

    public void OnCast(UnitStatManager stats)
    {
        var projectile = Instantiate(prefab) as GameObject;
        projectile.transform.position = origin.position;
        projectile.SendMessage("SetValues", stats.mAttack * 3f);
        projectile.GetComponent<Rigidbody>().velocity = origin.TransformDirection(Vector3.forward * force);
    }
}
