﻿using UnityEngine;
using System.Collections;

public class Skill : MonoBehaviour {

    public string skillName;
    public string description;
    public int skillId;
    public int levelReq;
    public int mpCost;
    public int cooldown;
    public Sprite skillIcon;
    public float skillToAnimDelay;
    private UnitStatManager stats;
    public float lastCast { get; private set; }

    //Still debating on whether gameobject or just a skill class in itself
    //lest there's a way to inject components on an existing class via database
    public Skill() { }
    public Skill (string Description,int SkillId, int LevelReq, int MpCost, int Cooldown, Sprite SkillIcon)
    {
        skillName = gameObject.name;
        description = Description;
        skillId = SkillId;
        levelReq= LevelReq;
        mpCost= MpCost;
        cooldown= Cooldown;
        skillIcon = SkillIcon;
    }

    void Start()
    {
        stats = GetComponentInParent<UnitStatManager>();
    }

    void Update()
    {
        ////tester statement for skill activation
        if (Input.GetKeyDown(KeyCode.Q))
            Use();
        ////
        Cooldown();
    }

    bool HasEnoughMana()
    {
        if (mpCost <= stats.CurrentMana)
            return true;
        else
            return false;
    }

    // code belongs in skill controller script
    public bool MeetsLevelReq()
    {
        if (stats.Level >= levelReq)
            return true;
        else
            return false;
    }

    public void Use()
    {
        if (lastCast <= 0)
        {
            lastCast = cooldown;
            if (HasEnoughMana())
            {
                Debug.Log("Cast (Skill)");
                stats.ManaUse(mpCost);
                StartCoroutine(Queue());
            }
        }
    }

    void Cooldown()
    {
        lastCast -= Time.deltaTime;
    }

    IEnumerator Queue()
    {
        BroadcastMessage("PlayAnim");
        yield return new WaitForSeconds(skillToAnimDelay);
        BroadcastMessage("OnCast", stats);
        BroadcastMessage("PlayFX");
    }
}
