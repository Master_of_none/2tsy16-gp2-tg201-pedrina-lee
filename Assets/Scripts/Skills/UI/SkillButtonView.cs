﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SkillButtonView : MonoBehaviour {

    private SkillListControl model;

    void Awake()
    {
        model = GameObject.FindGameObjectWithTag("Player").GetComponent<SkillListControl>();
    }

    void Update()
    {
        SetImage();
        CooldownView();
    }

    void SetImage()
    {
        GetComponent<Image>().sprite = model.activeSkill.GetComponent<Skill>().skillIcon;
    }

    public void CooldownView()
    {
        var lastCast = (int)model.activeSkill.GetComponent<Skill>().lastCast;
        if (lastCast <= 0)
        {
            lastCast = 0;
            GetComponentInChildren<Text>().text = " ";
        }
        else
        {
            GetComponentInChildren<Text>().text = lastCast.ToString();
        }
        //var image = GetComponent<Image>().fillAmount =lastCast;
    }
}
