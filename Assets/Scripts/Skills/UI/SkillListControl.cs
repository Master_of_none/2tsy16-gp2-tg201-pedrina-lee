﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillListControl : MonoBehaviour {

    private List<GameObject> skills;
    public GameObject activeSkill;

    void Awake()
    {
        
        var temp = gameObject.GetComponent<SkillListModel>();
        skills = temp.skillAccess;
    }

    void Start()
    {
        activeSkill = skills[0];
    }

    public void SetActiveSkill(string skillname)
    {
        foreach (GameObject obj in skills)
        {
            if (skillname + "(Clone)" == obj.name.ToString())
                if (obj.activeSelf)
                    activeSkill = obj;
        }
    }

    void Update()
    {
        SkillLevelReq();
    }

    public void SkillLevelReq()
    {
        foreach (GameObject skill in skills)
        {
            skill.SetActive(skill.GetComponent<Skill>().MeetsLevelReq());
        }
    }

    public void UseSkill()
    {
        //fix skill.use on skill script
        Debug.Log("Cast (SkillListControl)");
        Debug.Log(activeSkill.GetComponent<Skill>());
        activeSkill.GetComponent<Skill>().Use();
        //activeSkill.BroadcastMessage("Use");
    }
}
