﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SkillListModel : MonoBehaviour {

    public List<GameObject> skills;
    public List<GameObject> skillAccess;
    void Awake()
    {
        foreach (GameObject obj in skills)
        {
            var temp = Instantiate(obj) as GameObject;
            temp.transform.parent = gameObject.transform;
            skillAccess.Add(temp);
        }
    }

    void Start()
    {
        foreach (GameObject obj in skills)
        {
            Debug.Log(obj.name.ToString());
        }
    }
}
