﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class SkillListView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public GameObject slotPrefab;
    private SkillListModel list;
    void Awake()
    {
        list = GameObject.FindGameObjectWithTag("Player").GetComponent<SkillListModel>();
    }

    void Start()
    {
        var i = 0;
        foreach (GameObject skillPrefabs in list.skills)
        {
            var temp = Instantiate(slotPrefab) as GameObject;
            temp.transform.SetParent(gameObject.transform, false);
            
            temp.transform.position += new Vector3(0, i*-35,0 );
            i++;

            //temp.transform.position = new Vector3(x);
            temp.transform.FindChild("Skill Icon").GetComponent<Image>().sprite = skillPrefabs.GetComponent<Skill>().skillIcon;
            temp.transform.FindChild("Skill Name").GetComponent<Text>().text = skillPrefabs.GetComponent<Skill>().skillName;
            // align stats and shite
            var mpCost = skillPrefabs.GetComponent<Skill>().mpCost;
            var cooldown = skillPrefabs.GetComponent<Skill>().cooldown;
            temp.transform.FindChild("Skill Stats").GetComponent<Text>().text = "MP Cost: " + mpCost + "\nCooldown: " + cooldown;
            temp.transform.FindChild("Skill Description").GetComponent<Text>().text = skillPrefabs.GetComponent<Skill>().description;
            //temp.transform.FindChild("Skill Icon").gameObject.ima
            // transform references children of skill slot prefab and accesses texts plus image 
        }
        i = 0;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateBehaviour>().ToggleMovement();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStateBehaviour>().ToggleMovement();
    }

}
