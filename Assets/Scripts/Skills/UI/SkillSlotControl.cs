﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class SkillSlotControl : MonoBehaviour{

    private SkillListControl control;

    void Awake()
    {
        control = GameObject.FindGameObjectWithTag("Player").GetComponent<SkillListControl>();
    }

    public void Test()
    {   
        Debug.Log("Clicked");
        Debug.Log(gameObject.transform.FindChild("Skill Name").GetComponentInChildren<Text>().text.ToString());
        control.SetActiveSkill(gameObject.transform.FindChild("Skill Name").GetComponentInChildren<Text>().text.ToString());
    }
}
