﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseStat
{
    public StatType Type;
    public string Name;
    public float BaseValue;
    public float BonusValue;

    public enum StatType
    {
        Strength,
        Vitality,
        Intelligence,
        Spirit
    }

    public BaseStat() { }

    public BaseStat(StatType type, int baseValue)
    {
        Type = type;
        Name = type.ToString();
        BaseValue = baseValue;
        BonusValue = 0;
    }

    public float TotalValue
    {
        get { return BaseValue + BonusValue; }
    }
}