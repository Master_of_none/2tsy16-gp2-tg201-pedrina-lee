﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseStatManager : MonoBehaviour
{
    public int Level;

    public List<BaseStat> BaseStats;
    public List<DerivedStat> DerivedStats;

    public virtual void Start()
    {

    }

    public virtual void Update()
    {

    }

    public virtual void UpdateStats()
    {

    }
}