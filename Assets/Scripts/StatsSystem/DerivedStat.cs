﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DerivedStat
{
    public StatType Type;
    public string Name;
    public float BaseValue;
    public float BonusValue;
    public BaseStat ParentStat;

    public enum StatType
    {
        Health,
        Attack_Damage,
        Mana,
        Mana_Regen,
        Magic_Attack
    }

    public DerivedStat() { }
    public DerivedStat(StatType type, int baseValue, BaseStat baseStat)
    {
        Type = type;
        Name = type.ToString();
        BaseValue = baseValue;
        BonusValue = 0;
        ParentStat = baseStat;
    }

    public float TotalValue
    {
        get { return BaseValue + BonusValue; }
    }
}