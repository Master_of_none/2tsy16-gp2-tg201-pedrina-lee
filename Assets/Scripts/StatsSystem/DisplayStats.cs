﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DisplayStats : MonoBehaviour {
    
    public GameObject UI;
    public List<Text> MainStats;
    public List<Text> BaseStats;
    public List<Text> DerivedStats;
    private UnitStatManager unitStats;

    void Awake()
    {

    }

	// Use this for initialization
	void Start () {
        GetTextUI();
        unitStats = GetComponent<UnitStatManager>();
        print(unitStats);
        //UpdateStats();
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void UpdateStats()
    {
        foreach(BaseStat baseStat in unitStats.BaseStats)
        {
            foreach(Text statUI in BaseStats)
            {
                if (baseStat.Name == statUI.name)
                {
                    print(statUI);
                    //statUI.text = statUI.text + baseStat.TotalValue;
                    break;
                }
            }
        }
        print(unitStats.BaseStats);
        /*
        foreach(Text baseStat in BaseStats)
        {
            string temp = baseStat.text;
            GameObject tempStat = unitBaseStats.Find(x => x.GetComponent<BaseStat>().Name == baseStat.name);
            //BaseStat tempStat = unitBaseStats.
            //print(tempStat.name);
            //baseStat.text = temp + tempStat.TotalValue;
        }
        foreach (Text derivedStat in DerivedStats)
        {
            string temp = derivedStat.text;
            /*DerivedStat tempStat = unitDerivedStats.Find(x => x.Name.Contains(derivedStat.name));
            derivedStat.text = temp + tempStat.TotalValue;
        }*/
    }

    private void GetTextUI()
    {
        foreach (Transform textUI in UI.transform)
        {
            if (textUI.name.Contains("Base"))
            {
                foreach (Transform baseStats in textUI.FindChild("Base Stats"))
                {
                    BaseStats.Add(baseStats.GetComponent<Text>());
                }
            }
            else if (textUI.name.Contains("Derived"))
            {
                foreach (Transform derivedStats in textUI.transform)
                {
                    DerivedStats.Add(derivedStats.GetComponent<Text>());
                }
            }
        }
    }
}
