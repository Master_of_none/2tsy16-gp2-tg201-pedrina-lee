﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EnemyStatManager : BaseStatManager {

    public int Strength;
    public int Vitality;

    public float MaxHealth;
    public float CurrentHealth;

    public float MinExp;
    public float MaxExp;

    public Image HPBar;

    public float RandomExp()
    {
        return UnityEngine.Random.Range(MinExp, MaxExp);
    }

    private void SetupStats()
    {
        BaseStats.Add(new BaseStat(BaseStat.StatType.Strength, Strength));
        BaseStats.Add(new BaseStat(BaseStat.StatType.Vitality, Vitality));

        DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Attack_Damage,5, BaseStats[0]));
        DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Health,10, BaseStats[1]));
    }

	// Use this for initialization
	public override void Start () {
        BaseStats = new List<BaseStat>();
        DerivedStats = new List<DerivedStat>();
        SetupStats();

        MaxHealth = DerivedStats[1].TotalValue;
        CurrentHealth = MaxHealth;

	}

    // Update is called once per frame
    public override void Update () {
        UpdateHealth();
	}

    private void UpdateHealth()
    {
        HPBar.fillAmount = CurrentHealth / MaxHealth;
        if (CurrentHealth < 0)
            CurrentHealth = 0;
    }

    public void Damage(float damageValue)
    {
        CurrentHealth -= damageValue;
        GetComponent<Animator>().SetTrigger("Damage");
    }

    public bool IsDead()
    {
        return CurrentHealth <= 0;
    }
}
