﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerStatManager : BaseStatManager
{
    public int Strength;
    public int Vitality;
    public int Intelligence;
    public int Spirit;
    public int MaxExp;
    public int CurrentExp;
    public int StatPoints;
    public float CurrentHealth;
    public GameObject LevelUpAnim;
    public Image HPBar;
    public Slider ExpBar;

    private void SetupStats()
    {
        BaseStats.Add(new BaseStat(BaseStat.StatType.Vitality, Vitality));
        BaseStats.Add(new BaseStat(BaseStat.StatType.Strength, Strength));
        BaseStats.Add(new BaseStat(BaseStat.StatType.Intelligence, Intelligence));
        BaseStats.Add(new BaseStat(BaseStat.StatType.Spirit, Spirit));
        

        //DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Health, BaseStats[0]));
        //DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Attack_Damage, BaseStats[1]));
        //DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Magic_Attack, BaseStats[2]));
        //DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Mana, BaseStats[2]));
        //DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Mana_Regen, BaseStats[3]));
    }

    public override void UpdateStats()
    {
        //DerivedStats[0].BonusValue = DerivedStats[0].ParentStat.TotalValue * 10;
        //DerivedStats[1].BonusValue = (int)(DerivedStats[1].ParentStat.TotalValue / 2);
        //DerivedStats[2].BonusValue = (DerivedStats[2].ParentStat.TotalValue * Level) / 2;
        //DerivedStats[3].BonusValue = DerivedStats[2].ParentStat.TotalValue * 20;
        //DerivedStats[4].BonusValue = DerivedStats[3].ParentStat.TotalValue;
    }

    public override void Start()
    {
        BaseStats = new List<BaseStat>();
        DerivedStats = new List<DerivedStat>();

        Level = 1;
        SetupStats();
        UpdateStats();
        
        //CurrentHealth = (int)DerivedStats[0].TotalValue;
    }

    public override void Update()
    {
        UpdateBars();
    }

    private void UpdateBars()
    {
       //if (CurrentHealth <= 0)
       //    CurrentHealth = 0;
       //
       //HPBar.fillAmount = CurrentHealth / DerivedStats[0].TotalValue;
       //ExpBar.value = CurrentExp / MaxExp;
        
    }

    public void Damage(float damageValue)
    {
        CurrentHealth -= damageValue;
        GetComponent<Animator>().SetTrigger("Damage");
    }

    public void IncreaseStatValue(string statName)
    {
        if (StatPoints > 0)
        {
            foreach (BaseStat stat in BaseStats)
            {
                if (stat.Name == statName)
                {
                    stat.BonusValue++;
                    StatPoints--;
                    UpdateStats();
                    return;
                }
            }
        }
    }

    public void ReceiveExp(int value)
    {
        CurrentExp += value;
        if (CurrentExp >= MaxExp)
        {
            LevelUp();
        }
    }

    public void LevelUp()
    {
        Level++;
        StatPoints += 5;
        MaxExp *= 2;
        
        CurrentHealth = (int)DerivedStats[0].TotalValue;
        Destroy(Instantiate(LevelUpAnim, transform.position + new Vector3(0.0f, -1f), Quaternion.identity), .5f);
    }

    public void IsDead()
    {
        if(CurrentHealth <= 0)
        {
            GetComponent<PlayerStateBehaviour>().CurrentState = PlayerStateBehaviour.State.Dead;
        }
    }
}