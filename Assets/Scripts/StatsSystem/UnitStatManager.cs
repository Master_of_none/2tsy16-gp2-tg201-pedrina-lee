﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UnitStatManager : BaseStatManager
{
    public GameObject HealthBar;
    public GameObject ManaBar;
    public int Gold;
    public int Strength;
    public int Vitality;
    public int Intelligence;
    public int Spirit;
    public int MaxExp;
    public int CurrentExp;
    public int StatPoints;
    public float CurrentHealth;
    public float MaxHealth { get; private set; }
    public float CurrentMana;
    public float MaxMana { get; private set; }
    public float mAttack { get; private set; }
    public GameObject LevelUpAnim;
    //public Image HPBar;
    //public Slider ExpBar;

    private void SetupStats()
    {
        BaseStats.Add(new BaseStat(BaseStat.StatType.Strength, Strength));
        BaseStats.Add(new BaseStat(BaseStat.StatType.Vitality, Vitality));
        BaseStats.Add(new BaseStat(BaseStat.StatType.Intelligence, Intelligence));
        BaseStats.Add(new BaseStat(BaseStat.StatType.Spirit, Spirit));


        DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Attack_Damage, 5, BaseStats[0]));
        DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Health, 100, BaseStats[1]));
        DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Mana, 50, BaseStats[2]));
        DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Mana_Regen, 3, BaseStats[2]));
        DerivedStats.Add(new DerivedStat(DerivedStat.StatType.Magic_Attack, 3, BaseStats[3]));

    }

    public BaseStat FindBaseStat(string statName)
    {
        foreach(BaseStat stat in BaseStats)
        {
            if (stat.Name == statName)
            {
                return stat;
            }
        }
        return null;
    }

    public DerivedStat FindDerivedStat(string statName)
    {
        foreach(DerivedStat stat in DerivedStats)
        {
            if(stat.Name == statName)
            {
                return stat;
            }
        }
        return null;
    }

    public override void UpdateStats()
    {
        DerivedStats[0].BaseValue = BaseStats[0].TotalValue * 30;
        DerivedStats[1].BaseValue = (int)(BaseStats[1].TotalValue / 2);
        DerivedStats[2].BaseValue = BaseStats[2].TotalValue * 20;
        DerivedStats[3].BaseValue = BaseStats[3].TotalValue;
        DerivedStats[4].BaseValue = (int)(BaseStats[2].TotalValue * 3.5);
        MaxHealth = DerivedStats[0].TotalValue;
        MaxMana = DerivedStats[2].TotalValue;
        mAttack = DerivedStats[4].TotalValue;
    }

    public override void Start()
    {
        BaseStats = new List<BaseStat>();
        DerivedStats = new List<DerivedStat>();

        //Level = 1;
        SetupStats();
        UpdateStats();

        CurrentHealth = MaxHealth;
        CurrentMana = MaxMana;
    }

    public override void Update()
    {
        UpdateStats();
        RegenMana();
        LimitCheck();
        IsDead();
        HealthBar.GetComponent<Image>().fillAmount = CurrentHealth / MaxHealth;
        ManaBar.GetComponent<Image>().fillAmount = CurrentMana / MaxMana;
    }
    
    private void LimitCheck()
    {
        if (CurrentHealth > MaxHealth)
            CurrentHealth = MaxHealth;
        if (CurrentMana > MaxMana)
            CurrentMana = MaxMana;
    }

    void RegenMana()
    {
        CurrentMana += (DerivedStats[3].TotalValue * Time.deltaTime);
    }

    public void Damage(float damageValue)
    {
        CurrentHealth -= damageValue;
        GetComponent<Animator>().SetTrigger("Damage");
    }

    public void ManaUse(float manaCost)
    {
        CurrentMana-= manaCost;
    }

    //put in separate leveling script???
    public void IncreaseStatValue(string statName)
    {
        if (StatPoints > 0)
        {
            foreach (BaseStat stat in BaseStats)
            {
                if (stat.Name == statName)
                {
                    stat.BonusValue++;
                    StatPoints--;
                    UpdateStats();
                    return;
                }
            }
        }
    }

    public void ReceiveExp(int value)
    {
        CurrentExp += value;
        if (CurrentExp >= MaxExp)
        {
            LevelUp();
        }
    }

    public void ReceiveGold(int value)
    {
        Gold += value;
    }

    public void LevelUp()
    {
        Level++;
        StatPoints += 5;
        MaxExp *= 2;

        CurrentHealth = MaxHealth;
        // put in player statdisplay then simply sendmessage
        Destroy(Instantiate(LevelUpAnim, transform.position + new Vector3(0.0f, -1f), Quaternion.identity), 2f);
    }

    public int RandomExp()
    {
        return Random.Range(CurrentExp, MaxExp);
    }

    public void IsDead()
    {
        if (CurrentHealth <= 0)
        {
            if (gameObject.tag == "Player")
                GetComponent<PlayerStateBehaviour>().CurrentState = PlayerStateBehaviour.State.Dead;
            else
                GetComponent<StatePatternEnemy>().isDead = true;
        }
    }
}
