﻿using UnityEngine;
using System.Collections;

public interface IPlayerState {

    void ToIdleCombat();
    void ToCombat();
    void ToIdle();
    void ToAttack();
    void ToMovement();
    void ToChase();
    void UpdateState();
    void Raycast();
}
