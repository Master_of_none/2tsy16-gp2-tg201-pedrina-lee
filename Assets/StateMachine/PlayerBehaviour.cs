﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]

public class PlayerBehaviour : MonoBehaviour {

    public float AttackSpeed;
    public float AttackRange;
    public float MoveSpeed;
    public float RotateSpeed;
    public float Gravity;
    public float StoppingDistance;
    public bool BladeDrawn;
    public GameObject Cursor;
    public GameObject EnemySelect;
    public CharacterController PlayerController;
    public LayerMask Ground;
    public LayerMask Enemy;
    public Animator Anim;
    public Transform Target;
    
    [HideInInspector]
    public Vector3 TargetPosition;
    public IPlayerState CurrentState;
    public PlayerMoveState MoveState;
    public PlayerAttackState AttackState;
    public PlayerIdleState IdleState;
    public PlayerChaseState ChaseState;
    public PlayerCombatState CombatState;
    public PlayerIdleCombatState IdleCombatState;

    public void ToggleCursor(bool enemySelect)
    {
        if (!enemySelect)
            Cursor.SetActive(!Cursor.activeSelf);
        else
            EnemySelect.SetActive(!Cursor.activeSelf);
    }

	// Use this for initialization
	void Awake () {
        MoveState = new PlayerMoveState(this);
        AttackState = new PlayerAttackState(this);
        IdleState = new PlayerIdleState(this);
        ChaseState = new PlayerChaseState(this);
        IdleCombatState = new PlayerIdleCombatState(this);
        CombatState = new PlayerCombatState(this);

        PlayerController = GetComponent<CharacterController>();
        Anim = GetComponent<Animator>();
	}

    void Start()
    {
        TargetPosition = transform.position;
        CurrentState = IdleState;
    }
	
	// Update is called once per frame
	void Update () {
        CurrentState.UpdateState();
        EnemySelect.transform.position = Cursor.transform.position;
        print(CurrentState.GetType().ToString());
        PointBehaviour();
    }

    private void PointBehaviour()
    {
        EnemySelect.transform.position = Cursor.transform.position;
        if (Input.GetMouseButtonDown(0))
        {
            if (Target != null)
            {
                if (!EnemySelect.activeSelf)
                {
                    //EnemySelect.SetActive(!EnemySelect.activeSelf);
                }
                else if (EnemySelect.activeSelf)
                {

                }
            }
        }
    }
}
