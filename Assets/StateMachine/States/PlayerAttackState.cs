﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerAttackState : IPlayerState
{
    public readonly PlayerBehaviour player;
    private float lastAttack;

    public PlayerAttackState() { }
    public PlayerAttackState(PlayerBehaviour playerBehaviour)
    {
        player = playerBehaviour;
    }

    public void ToAttack()
    {
        Debug.Log("Cannot transition to the same state");
    }

    public void ToIdle()
    {
        player.CurrentState = player.IdleState;
    }

    public void ToMovement()
    {
        player.CurrentState = player.MoveState;
    }
    public void ToChase()
    {
        player.CurrentState = player.ChaseState;
    }

    public void ToCombat()
    {
        player.CurrentState = player.CombatState;
    }

    public void ToIdleCombat()
    {
        player.CurrentState = player.IdleCombatState;
    }

    public void UpdateState()
    {
        DrawBlade();
        Raycast();

        if(player.Target != null)
        {
            Attack();
            player.EnemySelect.transform.position = player.Target.transform.position;
        }
        else
        {
            ToMovement();
        }

        //player.Anim.
    }

    private void Attack()
    {
        Quaternion LookAt = Quaternion.LookRotation(player.Target.transform.position - player.transform.position);
        player.transform.rotation = Quaternion.Slerp(player.transform.rotation, LookAt, 10 * Time.deltaTime);

        if (Vector3.Distance(player.transform.position, player.Target.position) <= player.AttackRange)
        {
            if (Time.time > lastAttack)
            {
                lastAttack = Time.time + player.AttackSpeed;
                player.Anim.SetTrigger("Attack");
                player.Anim.SetBool("Moving", false);
            }
        }
        else
        {
            ToChase();
        }

    }

    private void Move()
    {
        Vector3 moveDirection = player.Target.position - player.transform.localPosition;
        moveDirection.Normalize();
        moveDirection.y = -player.Gravity;

        if (Vector3.Distance(player.transform.position, player.Target.position) > player.StoppingDistance)
        {
            Quaternion LookAt = Quaternion.LookRotation(player.Target.position - player.transform.position);
            LookAt.x = 0;
            LookAt.z = 0;
            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, LookAt, player.RotateSpeed * Time.deltaTime);
            player.PlayerController.Move(moveDirection * player.MoveSpeed * Time.deltaTime);
            player.Anim.SetBool("Moving", true);
        }
        else if (Vector3.Distance(player.transform.position, player.Target.position) <= player.StoppingDistance)
        {
            player.Anim.SetBool("Moving", false);
            moveDirection.Set(0, -player.Gravity, 0);
        }
    }

    public void Raycast()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, player.Enemy))
            {
                player.Target = hit.collider.gameObject.transform;
                player.Cursor.transform.position = hit.point + new Vector3(0.0f, 0.2f);
            }
            else if (Physics.Raycast(ray, out hit, Mathf.Infinity, player.Ground))
            {
                player.Target = null;
                player.TargetPosition = hit.point;
                player.Cursor.transform.position = hit.point + new Vector3(0.0f, 0.2f);
            }
        }
    }

    private void DrawBlade()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            player.BladeDrawn = !player.BladeDrawn;
            player.Anim.SetTrigger("DrawBlade");
            ToCombat();
        }
    }
}
