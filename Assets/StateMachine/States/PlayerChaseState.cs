﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerChaseState : IPlayerState
{
    public readonly PlayerBehaviour player;
    public PlayerChaseState() { }
    public PlayerChaseState(PlayerBehaviour playerBehaviour)
    {
        player = playerBehaviour;
    }

    public void Raycast()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, player.Enemy) && player.BladeDrawn)
            {
                player.Target = hit.collider.gameObject.transform;
                player.Cursor.transform.position = hit.point + new Vector3(0.0f, 0.2f);
            }
            else if (Physics.Raycast(ray, out hit, Mathf.Infinity, player.Ground))
            {
                player.Target = null;
                player.TargetPosition = hit.point;
                player.Cursor.transform.position = hit.point + new Vector3(0.0f, 0.2f);
                ToMovement();
            }
        }
    }

    public void ToAttack()
    {
        player.CurrentState = player.AttackState;
    }

    public void ToChase()
    {
        Debug.Log("Cannot transition into the same state");
    }

    public void ToIdle()
    {
        player.CurrentState = player.IdleState;
    }

    public void ToMovement()
    {
        player.CurrentState = player.MoveState;
    }

    public void ToCombat()
    {
        player.CurrentState = player.CombatState;
    }
    public void ToIdleCombat()
    {
        player.CurrentState = player.IdleCombatState;
    }

    public void UpdateState()
    {
        Raycast();
        Chase();
    }

    private void Chase()
    {
        if(player.Target != null)
        {
            Move();
        }
    }

    private void Move()
    {
        Vector3 moveDirection = player.Target.position - player.transform.localPosition;
        moveDirection.Normalize();
        moveDirection.y = -player.Gravity;

        if(Vector3.Distance(player.transform.position, player.Target.position) <= player.AttackRange)
        {
            ToAttack();
        }
        else if (Vector3.Distance(player.transform.position, player.Target.position) > player.StoppingDistance)
        {
            Quaternion LookAt = Quaternion.LookRotation(player.Target.position - player.transform.position);
            LookAt.x = 0;
            LookAt.z = 0;
            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, LookAt, player.RotateSpeed * Time.deltaTime);
            player.PlayerController.Move(moveDirection * player.MoveSpeed * Time.deltaTime);
            player.Anim.SetBool("Moving", true);
        }
        else if (Vector3.Distance(player.transform.position, player.Target.position) <= player.StoppingDistance)
        {
            player.Anim.SetBool("Moving", false);
            player.Cursor.SetActive(false);
            moveDirection.Set(0, -player.Gravity, 0);
            ToIdle();
        }
    }

    

}
