﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerCombatState : IPlayerState
{
    public readonly PlayerBehaviour player;
    private float armTime;
    private bool playerBladeDrawn;

    public PlayerCombatState() { }
    public PlayerCombatState(PlayerBehaviour playerBehaviour)
    {
        armTime = 1f + Time.time;
        player = playerBehaviour;
    }

    public void Raycast()
    {

    }

    public void ToAttack()
    {
        player.CurrentState = player.AttackState;
    }

    public void ToChase()
    {
        player.CurrentState = player.ChaseState;
    }

    public void ToIdle()
    {
        player.CurrentState = player.IdleState;
    }

    public void ToMovement()
    {
        player.CurrentState = player.MoveState;
    }

    public void ToCombat()
    {
        Debug.Log("Cannot transition into the same state");
    }
    public void ToIdleCombat()
    {
        player.CurrentState = player.IdleCombatState;
    }

    public void UpdateState()
    {
        player.BladeDrawn = !player.BladeDrawn;
        player.Anim.SetBool("BladeDrawn", player.BladeDrawn);
        if (player.BladeDrawn)
        {
            ToIdleCombat();
        }
        else
        {
            ToIdle();
        }
        return;

    }
}
