﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerIdleState : IPlayerState
{
    public readonly PlayerBehaviour player;
    public PlayerIdleState() { }
    public PlayerIdleState(PlayerBehaviour playerBehaviour)
    {
        player = playerBehaviour;
    }

    public void Raycast()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, player.Enemy))
            {


                player.Target = hit.collider.gameObject.transform;
                player.TargetPosition = player.Target.position + new Vector3(0.0f, 0.2f);
                player.Cursor.transform.position = hit.point;
                ToChase();
            }
            else if (Physics.Raycast(ray, out hit, Mathf.Infinity, player.Ground))
            {
                player.TargetPosition = hit.point;
                player.Target = null;
                player.Cursor.transform.position = hit.point + new Vector3(0.0f, 0.2f);
                ToMovement();
            }
        }
    }

    public void ToAttack()
    {
        player.CurrentState = player.AttackState;
    }

    public void ToChase()
    {
        player.CurrentState = player.ChaseState;
    }

    public void ToIdle()
    {
        Debug.Log("Cannot transition into the same state");
    }

    public void ToMovement()
    {
        player.CurrentState = player.MoveState;
    }

    public void ToCombat()
    {
        player.CurrentState = player.CombatState;
    }
    public void ToIdleCombat()
    {
        player.CurrentState = player.IdleCombatState;
    }

    public void UpdateState()
    {
        DrawBlade();
        Raycast();
    }

    private void DrawBlade()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            player.Anim.SetTrigger("DrawBlade");
            ToCombat();
        }
    }
}
