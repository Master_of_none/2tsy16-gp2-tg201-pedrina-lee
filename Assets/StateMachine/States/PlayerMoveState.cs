﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMoveState : IPlayerState
{
    public readonly PlayerBehaviour player;

    public PlayerMoveState() { }
    public PlayerMoveState(PlayerBehaviour playerBehaviour)
    {
        player = playerBehaviour;
    }

    public void ToAttack()
    {
        player.CurrentState = player.AttackState;
    }

    public void ToIdle()
    {
        player.CurrentState = player.IdleState;
    }

    public void ToMovement()
    {
        Debug.Log("Cannot transition into the same state");
    }
    public void ToChase()
    {
        player.CurrentState = player.ChaseState;
    }

    public void ToCombat()
    {
        player.CurrentState = player.CombatState;
    }

    public void ToIdleCombat()
    {
        player.CurrentState = player.IdleCombatState;
    }

    public void UpdateState()
    {
        Raycast();
        Move();
        if (player.Target != null)
        {
            ToChase();
        }
    }

    private void Move()
    {
        Vector3 moveDirection = player.TargetPosition - player.transform.localPosition;
        moveDirection.Normalize();
        moveDirection.y = -player.Gravity;

        if (Vector3.Distance(player.transform.position, player.TargetPosition) > player.StoppingDistance)
        {
            Quaternion LookAt = Quaternion.LookRotation(player.TargetPosition - player.transform.position);
            LookAt.x = 0;
            LookAt.z = 0;
            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, LookAt, player.RotateSpeed * Time.deltaTime);
            player.PlayerController.Move(moveDirection * player.MoveSpeed * Time.deltaTime);
            player.Anim.SetBool("Moving", true);
        }
        else if (Vector3.Distance(player.transform.position, player.TargetPosition) <= player.StoppingDistance)
        {
            player.Anim.SetBool("Moving", false);
            player.Cursor.SetActive(false);
            moveDirection.Set(0, -player.Gravity, 0);
            ToIdle();
        }
    }
    
    public void Raycast()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, player.Enemy) && player.BladeDrawn)
            {
                player.Target = hit.collider.gameObject.transform;
                player.Cursor.transform.position = hit.point + new Vector3(0.0f, 0.2f);
            }
            else if (Physics.Raycast(ray, out hit, Mathf.Infinity, player.Ground))
            {
                player.Target = null;
                player.TargetPosition = hit.point;
                player.Cursor.transform.position = hit.point + new Vector3(0.0f, 0.2f);
            }
        }
    }
}
